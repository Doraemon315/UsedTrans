package com.usd.serviceinter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.usd.bean.Commodity;
import com.usd.bean.LoginBean3;
import com.usd.bean.UserCommBean;



public interface IQueryAjax {
	public List<LoginBean3> queryAjax(LoginBean3 bean);
	public List<Commodity> queryAjax(String beanIphone);
	public List<Commodity> queryPerson(HttpServletRequest request);
	public List<UserCommBean> queryUserComm(HttpServletRequest request);
}
