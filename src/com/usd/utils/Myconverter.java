package com.usd.utils;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.apache.commons.beanutils.Converter;

public class Myconverter implements Converter {
	// 要转换的目标日期类型的格式
	private static final String DATE = "yyyy-MM-dd";
	private static final String DATETIME = "yyyy-MM-dd HH:mm";
	private static final String DATETIME1 = "yyyy-MM-dd HH:mm:ss";
	private static final String TIMESTAMP = "yyyy-MM-dd HH:mm:ss.SSS";

	@Override
	public <T> T convert(Class<T> arg0, Object arg1) {
		// TODO Auto-generated method stub
		return toDate(arg0, arg1);
	}

	/**
	 * 将字符串转换为对应格式的Date对象
	 * 
	 * @param type
	 *            转换的目标类型
	 * @param value
	 *            要转换的值,传进来的值
	 * @return Date对象
	 */
	public <T> T toDate(Class<T> type, Object value) {
		T t = null;
		if (value == null || value.equals("")) {
			// 传进来是空值则直接传回null值
			return null;
		}
		// 判断即将被转换的值是否是String类型，如果不是则直接传回
		if (value instanceof String) {
			//获取要进行转换的字符串,并去除中间的空格
			String str=	value.toString().trim();
			//获取要提取的字符串的长度
			int length=str.length();
			//判断即将转换的目标类型是否是Date类型
			if(type.equals(java.util.Date.class)) {
				try {
					DateFormat fmt=null;
					// 转换为“yyyy-MM-dd”格式的Date对象
					if (length <= 10) {
						fmt = new SimpleDateFormat(DATE, new DateFormatSymbols(Locale.CHINA));
					}else 
					// 转换为“yyyy-MM-dd HH:mm:ss”格式的Date对象
					if (length <= 16) {
						fmt = new SimpleDateFormat(DATETIME, new DateFormatSymbols(Locale.CHINA));
					}else
					// 转换为“yyyy-MM-dd HH:mm:ss”格式的Date对象
					if (length <= 19) {
						fmt = new SimpleDateFormat(DATETIME1, new DateFormatSymbols(Locale.CHINA));
					}else
					// 转换为“yyyy-MM-dd HH:mm:ss.SSS”格式的Date对象
					if (length <= 23) {
						fmt = new SimpleDateFormat(TIMESTAMP, new DateFormatSymbols(Locale.CHINA));
					}
					return(T) fmt.parse(str);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return (T)value;
	}
}
