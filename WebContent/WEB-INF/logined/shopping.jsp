<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">
	<head>
	<meta charset="UTF-8">
	<title>物品详情页</title>
	<link rel="stylesheet" type="text/css" href="static/css/succulent.css">
		<link rel="stylesheet" href="static/css/zzsc.css" />
		<link rel="stylesheet" href="static/src/jquery.skidder.css" />
		<link rel="stylesheet" type="text/css" href="static/lunbo/shopping.css"/>
		<link rel="stylesheet" href="static/bootstrap-3.3.7-dist/css/bootstrap.min.css">
		<script src="static/jquery/jquery.min.js"></script>
		<script src="static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<link href="static/lunbo/src/jquery.exzoom.css" rel="stylesheet" type="text/css"/>
	<style>
         #exzoom {
	     width: 400px;
  	     /*height: 400px;*/
	     margin: 20px auto;
         }
    </style>
	</head>
	
	<body>
		
		<header>
			<div class="logo" font-family: '微软雅黑'; "><p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/indexMain">Let</a></p></div>
			<div class="bar ">
				<form>
					<input type="text " placeholder="请输入您要搜索的内容... ">
					<button type="submit " id="button1 ">搜索</button>
				</form>
			</div>
			<span class="persons "><img src="${beanTpass }" /></span>
			<span class="usernamed ">${beanUser}</span>
		</header>
		
		<div>
		     <div class="container ">
		         <div class="row clearfix ">
		          	<div class="col-md-14 column ">
			        	<ul class="breadcrumb ">
					         <li>
						         <a href="# ">Home</a>
					         </li>
				           	 <li>
						          <a href="# ">Library</a>
					         </li>
					         <li class="active ">
						           Data
					        </li>
				        </ul>
			         </div>
		         </div>
	        </div>
	        
	        <div class="container">
            <div class="exzoom col-md-6" id="exzoom">
	        <!--大图区域-->
	            <div class="exzoom_img_box">
		             <ul class='exzoom_img_ul'>
			             <li><img src="${beanPicture} " /></li>
			             <li><img src="${beanPicture} " /></li>
			             <li><img src="${beanPicture} " /></li>
			             <li><img src="${beanPicture} " /></li>
			             <li><img src="${beanPicture} " /></li>
			             <li><img src="${beanPicture} " /></li>
		             </ul>
	             </div>
	             <!--缩略图导航-->
	             <div class="exzoom_nav"></div>
	             <!--控制按钮-->
	             <p class="exzoom_btn">
		             <a href="javascript:void(0);" class="exzoom_prev_btn"> &lt; </a>
		             <a href="javascript:void(0);" class="exzoom_next_btn"> &gt; </a>
	             </p>
            </div>
            
	    <div class="col-md-6" id="xxx-details">
		<div id="xxx-title"><h3>${beanComm }</h3><p>${beanIntroduce}</p></div>
		<div class="" id="xxx-price">
			<ul>
			<li id="hr-property">
				<span class="hr-property-type">价格</span>
				<span class="hr-property-price"><span>￥</span><span id="beanPrice">${beanPrice}</span></span>
			</li>
			<li id="hr-property">
				<span class="hr-attribute-type">专属价</span>
				<span class="hr-attribute-price"><span>￥</span><span>66.5-128.8</span></span>
			</li>
			<li id="hr-property">
				<span class="hr-freight">运费</span>
				<span class="hr-freight-type">包邮</span>
			</li>
		   </ul>
		</div>
		
		<div class="hr-parameter">
			<ul>
				<li><div class="hr-indcon-sales"><span>销量</span><span>188</span></div></li>
				<li><div class="hr-indcon-evaluate"><span>评价</span><span>88</span></div></li>
				<li><div class="hr-indcon-integral"><span>购买送积分</span><span>20</span></div></li>
			</ul>
		</div>
		
		<div class="" id="xxx-kind">
			<div>
				<dl>
					<dt class="xxx-kind-dd">宝贝评价</dt>
					<dd class="xxx-kind-ddd">
						<ul class="">
							<li><a href="#" role="button">超级棒</a></li>
							<li><a href="#" role="button">超级棒</a></li>
							<li><a href="#" role="button">超级棒</a></li>
							<li><a href="#" role="button">超级棒</a></li>
						</ul>
					</dd>
				</dl>
				<dl class="xxx-clear">
					<dt class="xxx-number">数量</dt>
					<dd class="xxx-number-d">
						<span class="xxx-amount-wiget"><input type="text" /></span>
						<span class="xxx-amount-btn">
							<span class="mui-amount-increase"></span>
						    <span class="mui-amount-decrease"></span></span>
						<span>件</span>
						<em class="xxx-amount-hidden" style="display: inline;">库存1</em>
					</dd>
				</dl>
				<div class="tb-action">
					<button type="button" class="btn btn-danger" style="background-color: #FFEDED; color: #FF0036;" onclick="pay_ajax()">立即购买</button>
					<button type="button" class="btn btn-danger" style="background-color: #FF0036;" onclick="join_shoppingcar(${beanCommid})">添加购物车</button>
				</div>
			</div>	
		</div>
		</div>
		<div>
			<div class="cl-line"></div>
		    <div class="cl-shopping">
		    	<div class="cl-shopping-line">-----看了又看-----</div>
		    	<ul class="cl-shopping-goods">
		    		<li>
		    			<span class="cl-goods-ima"><img src="static/lunbo/123/1.jpg"/></span>
		    			<span class="price"></span>
		    		</li>
		    		<li>
		    			<span class="cl-goods-ima"><img src="static/lunbo/123/2.jpg"/></span>
		    			<span class="price"></span>
		    		</li>
		    		<li>
		    			<span class="cl-goods-ima"><img src="static/lunbo/123/3.jpg"/></span>
		    			<span class="price"></span>
		    		</li>
		    	</ul>
		    </div>
		</div>
		
		</div>
		<div class="hot-line container"></div>
		<div class="footer">
			<div class="footer-line"></div>
		    <div class="footer-st"><span class="footer-title">Let</span></div>
		</div>
	</div>
</div>	

   <script type="text/javascript" src="static/js/jquery-1.7.1.js"></script>
    <script src="static/lunbo/src/jquery.exzoom.js"></script> 
    <script type="text/javascript">
	    $("#exzoom").exzoom({
	        autoPlay: false,
	    });//方法调用，务必在加载完后执行
	    //获取价格
		function pay_ajax() {
			       var bean=$("#beanPrice").html();

			 $.ajax({
				url : "PayAction",
				type : "POST",
				data : "beanPrice="+bean,
				dataType : "json", // 响应发挥的数据类型  
				success : function(data, Statstus) {
					//页面定向至
						window.location.href = "turnPay.jsp";
				},
				error : function(data) {
					alert("支付出现问题，你怕是没钱了吧大哥");
				}
			}); 
		}
		function join_shoppingcar(commid){
			 //使用jquery进行ajax处理	  		
		  				$.ajax({
		  					url : "JoinShoppingcarServle",//请求servlet映射地址
		  					type : "POST",//请求类型
		  					data:  "commid="+commid,
		  					success : function(data, textStatus, jqhr) {//由服务器返回的数据，描述请求结果，
		  						alert(data);
		  					},
		  					error : function(jqhr) {
		  						alert("赶快和强老板联系！！！！");
		  					}
		  				});
		  
		}
	</script>

</body>
</html>

