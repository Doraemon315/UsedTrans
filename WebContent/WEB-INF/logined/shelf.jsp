<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="static/layui/css/layui.css" media="all" />
<link rel="stylesheet" href="static/layui/css/layui.mobile.css" />
<script type="text/javascript" src="static/layui/lay/modules/form.js"></script>
<script src="static/layui/layui.all.js"></script>
<script src="static/layui/lay/modules/upload.js"></script>
<script src="static/layui/lay/modules/table.js"></script>
<script type="text/javascript" src="static/jquery/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="${basePath}static/paginationjs-master-2.1.0/dist/pagination.css" />
<script type="text/javascript"
	src="static/paginationjs-master-2.1.0/dist/pagination.min.js"
	charset="UTF-8"></script>
<!--引入bootstrap样式文件--->
<link rel="stylesheet" type="text/css"
	href="${basePath}static/bootstrap-3.3.7-dist/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="${basePath}static/paginationjs-master-2.1.0/dist/pagination.css" />
<link rel="stylesheet" type="text/css"
	href="${basePath}static/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />

<link
	href="${basePath}static/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" media="screen">
<script type="text/javascript">
	//使用jquery方ajax查询
	function doAjaxQuery() {
		$.ajax({
			url : "QueryUserServlet",
			type : "POST",
			//data : $("#userName").serialize(),//获取ID属性值序列化
			dataType : "json", // 响应发挥的数据类型  
			success : function(data, Statstus) {
				//调用分页函数
				do_page(data, 3);
			},
			error : function(data) {
				alert("查无此人！！！！！");
			}
		});
	}
</script>
<title>上架物品信息</title>
</head>

<body class="layui-layout-body">
	<div class="layui-layout layui-layout-admin">
		<div class="layui-header">
			<div class="layui-logo layui-bg-orange"><a href="${basePath}JumpServlet?jump=WEB-INF/logined/indexMain">二手交易网</a></div>
			<ul class="layui-nav  layui-layout-left">
				<li class="layui-nav-item"><a href="">最新活动(这是个假的！)</a></li>
				<li class="layui-nav-item"><a href="javascript:;">租赁(这是个假的！)</a>
					<dl class="layui-nav-child">
						<dd>
							<a href="">手机(这是个假的！)</a>
						</dd>
						<dd>
							<a href="">充电宝(这是个假的！)</a>
						</dd>
						<dd>
							<a href="">雨伞(这是个假的！)</a>
						</dd>
					</dl></li>
				<li class="layui-nav-item layui-this"><a href="${basePath}JumpServlet?jump=WEB-INF/logined/upload">上架</a></li>
				<li class="layui-nav-item"><a href="javascript:;">逛逛(里面都是假的！)</a>
					<dl class="layui-nav-child">
						<dd>
							<a href="">手机</a>
						</dd>
						<dd>
							<a href="">电脑</a>
						</dd>
						<dd class="layui-this">
							<a href="">游戏账号</a>
						</dd>
						<dd>
							<a href="">LOL</a>
						</dd>
						<dd>
							<a href="">DNF</a>
						</dd>
					</dl></li>
				<li class="layui-nav-item"><a href="">购物车(这是个假的！)</a></li>
				<li class="layui-nav-item"><a href="">社区(这是个假的！)</a></li>
			</ul>

			<ul class="layui-nav  layui-layout-right">
				<li class="layui-nav-item"><a href="javascript:;"><img
						src="${beanTpass}" class="layui-nav-img" />${beanUser}</a> <span
					class="layui-nav-more"></span>
					<dl class="layui-nav-child">
						<dd>
							<a href="">个人信息(这是个假的！)</a>
						</dd>
						<dd>
							<a href="">密码修改(这是个假的！)</a>
						</dd>
						<dd>
							<a href="${basePath}JumpServlet?jump=WEB-INF/logined/upload">上架物品</a>
						</dd>
					</dl></li>
				<li class="layui-nav-item"><a
					href="${basePath}JumpServlet?jump=WEB-INF/logined/index">注销</a></li>
			</ul>
		</div>
	</div>

	<fieldset class="layui-elem-field layui-field-title"
		style="margin-top: 20px;">
		<legend>我的发布</legend>
	</fieldset>

	<div>
		<blockquote class="layui-elem-quote">我已上架的物品</blockquote>
	</div>
	<form id="form1" class="form-horizontal" action="QueryServlet"
		method="post">
		<div class="col-sm-offset-2 col-sm-7">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>序号</th>
						<th>样式</th>
						<th>商品</th>
						<th>价格</th>
						<th>新旧程度</th>
						<th>商品介绍</th>
						<th>你现在到底卖不卖！</th>
					</tr>
				</thead>
				<tbody id="displaytbody">

				</tbody>
			</table>
		</div>
		<div class="col-sm-offset-2 col-sm-7">
			<div class="paginationjs paginationjs-big" id="pageQuery"></div>
		</div>
	</form>

	<script src="static/layui/lay/modules/element.js"></script>
	<script src="static/layui/lay/modules/table.js"></script>
	<script>
		//使用jquery方ajax查询
		function doAjaxQuery() {
			$.ajax({
				url : "QueryPerCommServlet",
				type : "POST",
				dataType : "json", // 响应发挥的数据类型  
				success : function(data, Statstus) {
					//调用分页函数
					do_page(data, 3);
				},
				error : function(data) {
					alert("查无此人！！！！！");
				}
			});
		}
		function do_page(data, pageSize) {
			$('#pageQuery')
					.pagination(
							{
								dataSource : data,//需要分页的数据
								pageSize : pageSize,//页面的大小
								showGoInput : true,//显示跳转页数输入框
								showGoButton : true,//显示跳转按钮
								callback : function(data, pagination) {
									//分页处理
									//拼凑html内容
									var strHtml = "";
									//data：要循环遍历的数据集合，index：循环中的下标，eachVal循环中index下标存放的值 
									$
											.each(
													data,
													function(index, eachVal) {
														strHtml += "<tr id='"
																+ (index)
																+ "'>";

														strHtml += "<th scope='row'>";
														strHtml += (index + 1);
														strHtml += "</th>";

														strHtml += "<td>";
														strHtml += "<img src='";
														strHtml+=eachVal.picture;					
														strHtml+="' width='50px' height='50px'/>";
														strHtml += "</td>";

														strHtml += "<td>";
														strHtml += eachVal.commodity;
														strHtml += "</td>";

														strHtml += "<td>";
														strHtml += eachVal.price;
														strHtml += "</td>";
														strHtml += "<td>";
														strHtml += eachVal.position;
														strHtml += "</td>";

														strHtml += "<td>";
														strHtml += eachVal.introduce;
														strHtml += "</td>";

														strHtml += "<td>";
														strHtml += "<a href='javascript:do_delete("
																+ (index)
																+ ","
																+ eachVal.commid
																+ ")'>不卖了！</a> "
														strHtml += "</td>";
														strHtml += "</tr>";

													});
									$("#displaytbody").html(strHtml);
								}
							})
		}
		window.onload = doAjaxQuery;
		function do_delete(index, commid) {
			var displaytbody = document.getElementById("displaytbody")
					.deleteRow(index);
			//使用jquery进行ajax处理	  		
			$.ajax({
				url : "DeleteCommServlet",//请求servlet映射地址
				type : "POST",//请求类型
				data : "commid=" + commid,
				success : function(data, textStatus, jqhr) {//由服务器返回的数据，描述请求结果，
					alert("不卖就算了，这辣鸡东西没人会买！");
				},
				error : function(jqhr) {
					alert("下架失败,赶快和强老板联系！！！！");
				}
			});

		}
		layui.use('element', function() {
			var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

			//监听导航点击
			element.on('nav(demo)', function(elem) {
				//console.log(elem)
				layer.msg(elem.text());
			});
		});
	</script>
</html>
<script type="text/javascript" src="static/layui/lay/modules/form.js"></script>