package com.usd.trans;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usd.service.VerLogin;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class DeleteCommServlet
 */
@WebServlet("/DeleteCommServlet")
public class DeleteCommServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteCommServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 设置编码集
		response.setContentType("text/html;charset=utf-8");
		// 获取request域中的数据
		String commid = request.getParameter("commid");
		// 进行删除下架操作
		int deleteLine = Myutils.getInstence(VerLogin.class).deleteCommInfo(commid);
		
		if (deleteLine > 0) {
			// 回调函数传回参数
			response.getWriter().print("下架成功,没人再买得到这件破烂了");
		} else {
			response.getWriter().print("下架失败，刷新再说吧！！");
		}

	}

}
