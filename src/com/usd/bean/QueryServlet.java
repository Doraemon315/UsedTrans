package com.usd.bean;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.usd.service.QueryAjaxImpl;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class LoginServlet3
 */
@WebServlet("/QueryServlet")
public class QueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QueryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 设置返回文本的编码格式
		response.setContentType("text/html;charset=utf-8");
		String beanIphone=(String)request.getSession().getAttribute("beanIphone");
		// 查询匹配到的数据存入集合当中
		List<Commodity> list = Myutils.getInstence(QueryAjaxImpl.class).queryAjax(beanIphone);
		// 借助第三方的工具将集合转换为json字符串【alibaba的fastjson插件包】
		// 将查询结果集list转换为json字符串
		String strJson = JSON.toJSONStringWithDateFormat(list, "yyyy-MM-dd HH:mm:ss");
		System.out.println(strJson);
		System.out.println(strJson.length());
		PrintWriter out = null;
		if (strJson.length() >2) {
			// 将数据返回请求端回调函数中
			out = response.getWriter();
			out.print(strJson);
			out.flush();
			out.close();
		} else {
			out = response.getWriter();
			out.print("暂无商品上架");
			out.flush();
			out.close();
		}

	}

}
