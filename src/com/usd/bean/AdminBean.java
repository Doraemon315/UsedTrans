package com.usd.bean;

import java.util.Date;

public class AdminBean {
	private String adminName;
	private String adminSex;
	private String adminPasd;
	private String adminUrl;
	private Date adminDate;

	public AdminBean() {
		super();
	}

	public AdminBean(String adminName, String adminSex, String adminPasd, String adminUrl, Date adminDate) {
		super();
		this.adminName = adminName;
		this.adminSex = adminSex;
		this.adminPasd = adminPasd;
		this.adminUrl = adminUrl;
		this.adminDate = adminDate;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getAdminSex() {
		return adminSex;
	}

	public void setAdminSex(String adminSex) {
		this.adminSex = adminSex;
	}

	public String getAdminPasd() {
		return adminPasd;
	}

	public void setAdminPasd(String adminPasd) {
		this.adminPasd = adminPasd;
	}

	public String getAdminUrl() {
		return adminUrl;
	}

	public void setAdminUrl(String adminUrl) {
		this.adminUrl = adminUrl;
	}

	public Date getAdminDate() {
		return adminDate;
	}

	public void setAdminDate(Date adminDate) {
		this.adminDate = adminDate;
	}

	@Override
	public String toString() {
		return "AdminBean [adminName=" + adminName + ", adminSex=" + adminSex + ", adminPasd=" + adminPasd
				+ ", adminUrl=" + adminUrl + ", adminDate=" + adminDate + "]";
	}

}
