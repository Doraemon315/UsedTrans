package com.usd.db;

import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory;


/**
 * @author Ming 单例模式实现数据源
 */
public class DbDataSource {
	// 私有化数据源属性
	private static DataSource ds = null;

	
	// 私有化构造器
	private DbDataSource() {

	}

	public static DataSource getDataSource() {
		
		if (ds == null) {
			// 使用Properties读取配置信息
			Properties pro = new Properties();
			try {
				// 加载配置信息
				pro.load(DbDataSource.class.getClassLoader().getResourceAsStream("db.properties"));

				// 使用工厂类初始化数据，数据源
				ds = BasicDataSourceFactory.createDataSource(pro);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.getMessage();
			}

		}
		// 返回一个DataSource数据源
		return ds;
	}
	public static void main(String[] args) {
/*		// 创建一个QueryRunner类 -- 等价于jdbc的statement查询器,第一个参数是数据源对象，第二个参数是否支持占位符
		QueryRunner qr= new QueryRunner(getDataSource(),true);
		//写入查询的sql语句
		String sql="select * from mvctest";
		//查询操作，并把值写入Bean对象， 一起装进List中，并返回List对象
		try {
			List<LoginBean2> list=qr.query(sql, new BeanListHandler<LoginBean2>(LoginBean2.class));
			//遍历查询结果
			for (LoginBean2 loginBean2 : list) {
				System.out.print(loginBean2);
				System.out.println();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
}
