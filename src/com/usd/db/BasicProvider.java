package com.usd.db;

import java.sql.SQLException;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;



public class BasicProvider {
	/**
	 * @param sql
	 * @param rsh
	 * @param parm
	 * @return 返回<T>集合对象
	 */
	public <T> T query(String sql, ResultSetHandler<T> rsh, Object... parm) {
		T t = null;
		// 创建一个queryrunner类 -- 等价于jdbc的statement查询器
		QueryRunner qr = new QueryRunner(DbDataSource.getDataSource(), true);
		try {
			// 返回值根据ResultSetHandler的参数决定，此时返回的是List对象集合
			t = qr.query(sql, rsh, parm);
		} catch (SQLException e) {
			// TODO Auto-generated catch block	
			e.printStackTrace();
		}
		// 此时返回List集合对象
		return t;

	}
	/**
	 * 修改、新增、删除操作
	 * 
	 * @param sql    sql语句
	 * @param params 参数列表
	 * @return 操作结果
	 */
	public int upade(String sql, Object... params) {
		// 返回结果
		int i = 0;
		try {
			// 创建一个queryrunner类 -- 等价于jdbc的statement查询器
			QueryRunner qr = new QueryRunner(DbDataSource.getDataSource(), true); // 第二个参数是支持sql语句中使用占位符
			// 进行修改、新增、删除操作
			//将sql语句插入更新语句，将要给占位符赋值的参数插入
			i = qr.update(sql, params);
		} catch (Exception e) {
			// 查询操作异常
			System.out.println("查询操作异常：" + e.getMessage());
			e.printStackTrace();
		}
		// 返回值
		return i;
	}
}
