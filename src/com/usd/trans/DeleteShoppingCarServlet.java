package com.usd.trans;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usd.service.VerLogin;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class DeleteShoppingCarServlet
 */
@WebServlet("/DeleteShoppingCarServlet")
public class DeleteShoppingCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteShoppingCarServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 设置返回文本的编码格式
		response.setContentType("text/html;charset=utf-8");
		String shopid=(String)request.getParameter("shopid");
		int deleteLine=Myutils.getInstence(VerLogin.class).deleteShoppingCar(shopid);
		PrintWriter out=response.getWriter();
		if(deleteLine>0) {
			out.print("删除成功");
		}else {
			out.print("删除失败");
		}
	}

}
