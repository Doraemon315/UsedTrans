package com.usd.filter;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2016092100564014";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC0FqcCeTzlP1iOfq6+HO4pK+I5/epeQ3YAnsWujmG9levWcfjL+irAFD+OvbECxD3vOOuQVQLshxKnX5/ETURL0QFM1PBX1NfCDsivsN8NoylhBS18oD+i9R7T8q3dKTmLoO5yPDSgMqwVv61/Lo/2DbX1VIKTZfYf52EptNZGCSUmoXTJXJ1sl9lITtIX3/kfpoWJjO6sSr6R+3oxR9zkH7a9P2qJxuRnuX3Y8qXsIJ0n6LQB5PcY2Q+A7P6hizi7r4V2Rva+VfKdNQJ2wwSaDBWPXzL96ArLyPOt7IDdvl7DeEaQ+o4FbEYZGQIoTMb6vK2kOkHUFjh/B5bPBGHPAgMBAAECggEAILmBDbUEONBDAlnI3SkaqgacbFdUsF5n4gL3/xlGb5H/sg8x27cQ76h+SCz2Ln+ElaTtQWQBTQPagumgeaZkw7YuG00Sg5NcxZm0JCDq12qPyV6MfnoMNkt+l5sgtvCX0WSkD8Qa+AVdOfKDz3mhlV+2cecydyBvEisUf+Spjz4pQ0fz50RZ5hWJDBrddKxS4YO3MHiHkwGj1IqkzrQVgKUW1qX+kUxhaNcs3gps7EwMr1BSj/tbQnd6IMEnELTPNvWMUPBFR2kKJU26PNubIPNx+F0c57lowj9mvF08mdBDRlkt/1OVWQwiwnaLDe9etgE9SeJa94B7la7FhAYccQKBgQD5m1crGw224sCKK6ISQg4bmCPX6C8tUZxt/RQOYLzAmkJhHhwvFH3Y2v45iUowyiC/QtNe9k/3q618HSIoohH1PFJKMl0x3sq/9zqhBDXxYpBezTK2lxUBLhL3giVEaZ99azdv7eoNyHKVVR9Tyza3ZMI/mlxM7DjxqhWGlzwOlwKBgQC4s3vVOv8PPkAk1iAsNU+xbBx9sIF1snImZuRA8YVo18VQCcjbYnomKE87g7dE8aef6b90+3YlMwc78AzKZJhe3goI443NkImuX7sP8TX+X/LTrNj9BI7MOtiCLyzfIwoR5FBO8O7ovWfXiDV+i6NdLfrf/O8SKlJHiaLd1blliQKBgDHwWmhNaVkF1O/P+wUcIiZzMfiog4fVpC59S930jM8cDdvQYmMo/mS8q9BnH2jTh+m8PP3q2eT7fCIw0fy/MEiYYP5nRS4NHRN7K0e7x+8bgTb2ZwcRAyrRRQowB7TATSCSquzB9On9CK/H8BSbu77Fcfe3sm/RczMmjFf5OhLRAoGAQPpjxlBLCwHLbyolh2f58WEnZNCDuvRcVEY+ZtqRJKFEOTQ4BXZPorzrovRjtU2OuyNwx6bvmYaqNNvupAVf1YAn55imensu9LmZBbLGXAdFVlKow9qmohLRuHsAPkAFeuXnmt4ILrjzn0+8Ahj+D/ntGH/TW4CoEaLKOWDHKIECgYEA4Oyt9rhpNDivAHJlCwzD7h0kp8/NyZHINnsdlJueUpRsYeZR8VEUreEyhYiFfRWkVJKZD4miSaGNC7osvSxWuh+JZ7TPhMtC+uuXcZehNADjEM1REy6CbRbDIy+fDCPpkvjsXp6UFZxG0DMC93tojPoiWR8SsrftnMaOp0SCXZg=";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtBanAnk85T9Yjn6uvhzuKSviOf3qXkN2AJ7Fro5hvZXr1nH4y/oqwBQ/jr2xAsQ97zjrkFUC7IcSp1+fxE1ES9EBTNTwV9TXwg7Ir7DfDaMpYQUtfKA/ovUe0/Kt3Sk5i6Ducjw0oDKsFb+tfy6P9g219VSCk2X2H+dhKbTWRgklJqF0yVydbJfZSE7SF9/5H6aFiYzurEq+kft6MUfc5B+2vT9qicbkZ7l92PKl7CCdJ+i0AeT3GNkPgOz+oYs4u6+Fdkb2vlXynTUCdsMEmgwVj18y/egKy8jzreyA3b5ew3hGkPqOBWxGGRkCKEzG+rytpDpB1BY4fweWzwRhzwIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/return_url.jsp";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

