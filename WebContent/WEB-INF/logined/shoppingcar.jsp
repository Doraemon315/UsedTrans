<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link type="text/css" rel="stylesheet" href="static/css/base.css">
<link rel="stylesheet" type="text/css" href="static/css/css.css">
<title>购物车</title>
<script type="text/javascript" src="static/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="static/js/common.js"></script>
<script type="text/javascript">
//使用jquery方ajax查询
function doAjaxQuery() {
	$.ajax({
		url : "QueryShoppingServlet",
		type : "POST",
		//data : $("#userName").serialize(),//获取ID属性值序列化
		dataType : "json", // 响应发挥的数据类型  
		success : function(data, Statstus) {
			//分页处理
			//拼凑html内容
			var strHtml = "";
			var price=0;
			//data：要循环遍历的数据集合，index：循环中的下标，eachVal循环中index下标存放的值 
			$
					.each(
							data,
							function(index, eachVal) {
								
								strHtml +="<li id='deletemain'>";
								strHtml +="<table "+index+">";
								strHtml +="<tr>";
								strHtml +="<td width='15%'><img src='";
								strHtml +=eachVal.picture;
								strHtml +="'width='52' height='44'></td>";
								strHtml +="<td width='65%'><p>";
								strHtml +=eachVal.commodity+"</p>";
								strHtml +="<p><span class='lse'>价格：￥";
								strHtml +=eachVal.price;
								strHtml +="</span> <span class='pl15'>数量：1 件</span></p>";
								strHtml +="</td>";
								strHtml +="<td width='10%' valign='middle'><a href='javascript:do_delete("+index+","+eachVal.shopid+")' class='del' id='shanchu2' >删除</a></td>";
								strHtml +="<td width='10%'> <input type='checkbox' class='checkbox'  name='check_item' id='check_item'></td>";
								strHtml +="</tr>";
								strHtml +="</table>";
								strHtml +="</li >";
								price+=eachVal.price;
							});
			$("#shoppingcar").html(strHtml);
			$("#beanPrice").text(price);
		},
		error : function(data) {
			
		}
	});
}
 
window.onload = doAjaxQuery;

function do_delete(index,shopid){
	var displaytbody = document.getElementById("deletemain").remove(index);
	 //使用jquery进行ajax处理	  		
 				$.ajax({
 					url : "DeleteShoppingCarServlet",//请求servlet映射地址
 					type : "POST",//请求类型
 					data:  "shopid="+shopid,
 					success : function(data, textStatus, jqhr) {//由服务器返回的数据，描述请求结果，
 						alert(data);
 					},
 					error : function(jqhr) {
 						alert("下架失败,赶快和强老板联系！！！！");
 					}
 				});
}

</script>
</head>
<body>
<div class="wrap" id="zhengti">
	<div class="top">
        <a href="index.html" class="aleft"></a>
        <h1>购物车</h1>
        <a href="#" class="aright"></a>
	</div>
<form action="PayAction" method="post" enctype="multipart/form-data"> 
<div class="container">
	<div class="cart clear">
   		<div class="shadow">
        <table width="100%" border="0">
            <tr>
                <td width="85%">
                  <h3 class="fl">购买到的产品</h3></li>
                </td>
                <td width="10%"> <input type="checkbox" class="checkbox"  name="check_all" id="check_all" > </td>
            </tr>
        </table>
     
       
       <div class="c_list cb">
       	<ul id="shoppingcar">
        	<li >
            	<table>
                	<tr>
                    	<td width="15%"><img src="static/images/cart_img.jpg" width="52" height="44"></td>
                        <td width="65%">
                        	<p>仙人掌</p>
                            <p><span class="lse">￥18.00</span> <span class="pl15">数量：<input type="text" name="num" class="num"> 棵</span></p>
                        </td>
                        <td width="10%" valign="middle"><a href="#" class="del" id="shanchu1">删除</a></td>
                        <td width="10%"> <input type="checkbox" class="checkbox"  name="check_item" id="check_item"></td>
                    </tr>
                </table>
              </li>
             
              <li > 
               <table>
                	<tr>
                    	<td width="15%"><img src="static/images/cart_img.jpg" width="52" height="44"></td>
                        <td width="65%">
                        	<p>狗尾草</p>
                            <p><span class="lse">￥19.00</span> <span class="pl15">数量：<input type="text" name="num" class="num"> 棵</span></p>
                        </td>
                        <td width="10%" valign="middle"><a href="#" class="del" id="shanchu2">删除</a></td>
                        <td width="10%"> <input type="checkbox" class="checkbox"  name="check_item" id="check_item"></td>
                    </tr>
                </table>
                </li>
               
               <li >
               <table>
                	<tr>
                    	<td width="15%"><img src="static/images/cart_img.jpg" width="52" height="44"></td>
                        <td width="65%">
                        	<p>食人花</p>
                            <p><span class="lse">￥198.00</span> <span class="pl15">数量：<input type="text" name="num" class="num"> 棵</span></p>
                        </td>
                        <td width="10%" valign="middle"><a href="#" class="del" id="shanchu3">删除</a></td>
                        <td width="10%"> <input type="checkbox" class="checkbox"  name="check_item" id="check_item"></td>
                    </tr>
                </table>
                </li>
        </ul>
       </div>
       
       
    </div>  
    </div>
</div>

<div class="cart_bg">
<table width="100%" border="0">
    <tr>
    	<td width="3%"></td>
        <td width="25%"><input type="checkbox" class="checkbox" id="box_all" checked="checked"> 全选</td>
        <td width="49%">
            <p> <span class="pl15">金额合计：</span><span class="ff6" name="totalmoney"  id="beanPrice"></span></p>
        </td>
        <td width="20%"><button class="submit" type="button" onclick="pay_ajax()">去结算</button></td>
        <td width="3%"></td>
    </tr>
</table>
</div> 


</form>
<div class="shop_cart" >
<a href="#">
	<img src="static/images/shop_cart.png" class="cart_img">
    <p>购物车</p>
</a>
</div>  
    
<footer>
<div class="h50"></div>
	<ul class="bottom_nav w tc">
        <li><a href="index.html">返回</a></li>
        <li><a href="#">一键拨号</a></li>
        <li><a href="index.html"></a></li>
        <li><a href="#">商品分类</a></li>
        <li><a href="#">搜索</li>
	</ul>
</footer>
</div>
<div style="text-align:center;">
<p id="footfont">确认选中后再结算！</p>
</div>
</body>
<script type="text/javascript">
 function pay_ajax() {
    var test1=$("#beanPrice").html();
    

$.ajax({
	url : "PayAction",
	type : "POST",
	data : "beanPrice="+test1,
	dataType : "json", // 响应发挥的数据类型  
	success : function(data, Statstus) {
		//页面定向至
			window.location.href = "turnPay.jsp";
	},
	error : function(data) {
		alert("支付出现问题，你怕是没钱了吧大哥");
	}
}); 
} 
</script>
<script type="text/javascript" src="static/js/jquery-1.7.1.js"></script>
</html>
