<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!--引入bootstrap样式文件--->
<link rel="stylesheet" type="text/css"
	href="${basePath}static/bootstrap-3.3.7-dist/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css"
	href="${basePath}static/paginationjs-master-2.1.0/dist/pagination.css" />
<link rel="stylesheet" type="text/css"
	href="${basePath}static/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />

<link
	href="${basePath}static/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" media="screen">
<style>
body {
	margin: 0px;
	padding: 0px;
}

#mainBody {
	width: 95%;
	margin: 0px auto;
}
</style>
<!--引入jquery-->
<script type="text/javascript"
	src="${basePath}static/js/jquery-1.12.4/jquery-1.12.4.min.js"></script>
<!--引入bootstrap脚本--->
<script type="text/javascript"
	src="${basePath}static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${basePath}static/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js"
	charset="UTF-8"></script>
<script type="text/javascript"
	src="${basePath}static/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js"
	charset="UTF-8"></script>
<script type="text/javascript"
	src="static/paginationjs-master-2.1.0/dist/pagination.min.js"
	charset="UTF-8"></script>
<script type="text/javascript">
	//使用jquery方ajax查询
	function doAjaxQuery() {
		$.ajax({
			url : "QueryInfoServlet",
			type : "POST",
			//data : $("#userName").serialize(),//获取ID属性值序列化
			dataType : "json", // 响应发挥的数据类型  
			success : function(data, Statstus) {
				//调用分页函数
				do_page(data, 3);
			},
			error : function(data) {
				alert("查无此人！！！！！");
			}
		});
	}
	
	function do_page(data, pageSize) {
		$('#pageQuery')
				.pagination(
						{
							dataSource : data,//需要分页的数据
							pageSize : pageSize,//页面的大小
							showGoInput : true,//显示跳转页数输入框
							showGoButton : true,//显示跳转按钮
							callback : function(data, pagination) {
								//分页处理
								//拼凑html内容
								var strHtml = "";
								//data：要循环遍历的数据集合，index：循环中的下标，eachVal循环中index下标存放的值 
								$
										.each(
												data,
												function(index, eachVal) {
													strHtml += "<tr id='"+(index)+"'>";

													strHtml += "<th scope='row'>";
													strHtml += (index + 1);
													strHtml += "</th>";
													
													strHtml += "<td>";
													strHtml += "<img src='";
													strHtml+=eachVal.tpass;					
													strHtml+="' width='50px' height='50px'/>";
													strHtml += "</td>";
													
													strHtml += "<td>";
													strHtml += eachVal.iphone;
													strHtml += "</td>";

													strHtml += "<td>";
													strHtml += eachVal.userName;
													strHtml += "</td>";

													strHtml += "<td>";
													strHtml += eachVal.sex;
													strHtml += "</td>";
													
													strHtml += "<td>";
													strHtml += eachVal.birthday;
													strHtml += "</td>";
													
													strHtml += "<td>";
													strHtml += "<a href='javascript:do_delete("+(index)+","+eachVal.iphone+")'>删除</a> "
													strHtml += "</td>";
													strHtml += "</tr>";

												});
								$("#displaytbody").html(strHtml);
							}
						})
	}
	window.onload=doAjaxQuery;
	
	function do_delete(index,iphone){
		var displaytbody = document.getElementById("displaytbody").deleteRow(index);
		 //使用jquery进行ajax处理
	  
	  		
	  				$.ajax({
	  					url : "DeleteUserServlet",//请求servlet映射地址
	  					type : "POST",//请求类型
	  					data:  "iphone="+iphone,
	  					success : function(data, textStatus, jqhr) {//由服务器返回的数据，描述请求结果，
	  						alert(data);

	  					},
	  					error : function(jqhr) {
	  						alert("删除失败，请刷新");
	  					}
	  				});
	  
	}
	</script>
	</head>
<body>
<a ></a>
<form id="form1" class="form-horizontal" action="QueryServlet"
		method="post">
	<div class="form-group">
		<div class="col-sm-offset-5 col-sm-2">
			<!-- <input id="userName" name="userName" type="text">
			<button type="button" class="btn btn-default" onclick="doAjaxQuery()">查询</button> -->
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-7">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>序号</th>
						<th>头像</th>
						<th>账户Id</th>
						<th>账户名</th>
						<th>性别</th>
						<th>生日</th>
						<th>天秀的操作</th>
					</tr>
				</thead>
				<tbody id="displaytbody">

				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-offset-2 col-sm-7" >
		<div class="paginationjs paginationjs-big" id="pageQuery">
		</div>
	</div>
	</form>
</body>
</html>