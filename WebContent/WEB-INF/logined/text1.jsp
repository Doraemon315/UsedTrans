<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="static/css/succulent.css" />
		<link rel="stylesheet" href="static/css/test1.css" />
		<link rel="stylesheet" href="static/css/pageSwitch.min.css" />
		<link rel="stylesheet" href="static/css/diduan.css" />
		<title></title>
	</head>

	<body>
		<header>
			<div class="logo" style="margin-top: 24px;  font-family: '微软雅黑';"><p>Let</p></div>
			<div class="bar">
				<form>
					<input type="text" placeholder="请输入您要搜索的内容...">
					<button type="submit" id="button1">搜索</button>
				</form>
			</div>
			<span class="persons"><img src="static/images/1.jpg" /></span>
			<span class="usernamed" style="margin-top: 40px;">用户名</span>
		</header>
		<div class="toolbar">
			<div id="open-sb" class="menu-button menu-left"></div>
		</div>

		<section class="sidebar">
			<div class="subNav">
				<h5>菜单</h5></div>
			<hr />
			<div class="subNav">
				<a href="${basePath}JumpServlet?jump=WEB-INF/logined/indexMain"><h6>主页</h6></a></div>
			<div class="subNav">
				<h6>新品推荐</h6></div>
			<ul class="navContent">
				<li>
					<a href="#">当季新品</a>
				</li>
			</ul>
			<div class="subNav">
				<h6>书</h6></div>
			<div class="subNav">
				<h6>服装</h6></div>
			<div class="subNav">
				<h6>手机</h6></div>
			<ul class="navContent">
				<li>
					<a href="#">安卓</a>
				</li>
				<li>
					<a href="#">IPHONE</a>
				</li>
				<li>
					<a href="#">其他</a>
				</li>
			</ul>
			<div class="subNav">
				<h6>车</h6></div>
			<ul class="navContent">
				<li>
					<a href="#">大巴</a>
				</li>
				<li>
					<a href="#">轿车</a>
				</li>
				<li>
					<a href="#">摩托</a>
				</li>
			</ul>
			<div class="subNav">
				<h6>代金卷</h6></div>
			<div class="subNav">
				<h6>关于我们</h6></div>
			<ul class="navContent">
				<li>
					<a href="#">使命宣言</a>
				</li>
			</ul>
			<div class="subNav">
				<h6>帮助中心</h6></div>
			<ul class="navContent">
				<li>
					<a href="#">常见问题</a>
				</li>
				<li>
					<a href="#">联系我们</a>
				</li>
				<li>
					<a href="#">使用条款</a>
				</li>
				<li>
					<a href="#">隐私权政策</a>
				</li>
			</ul>
		</section>
		<div id="container">
			<div class="sections">
				<div class="section" id="section0">
					<h3>this is the page1</h3></div>
				<div class="section" id="section1">
					<h3>this is the page2</h3></div>
				<div class="section" id="section2">
					<h3>this is the page3</h3></div>
				<div class="section" id="section3">
					<h3>this is the page4</h3></div>
			</div>
		</div>
		<div class="maindiv">
			<button class="leftdiv">查看商品</button>
			<div class="centerdiv"></div>
			<a href="upload.html">
					<button class="rightdiv">出售</button>
				</a>
		</div>
		<div class="mission">
    <div class="container">
        <div class="mission-header">
            <h3>今日推荐</h3>
        </div>
        <div class="mission-container">
            <div class="mission_div mission-rig">
                <a href="#"><img src="static/imag/10.jpg" alt=""/></a>
            </div>
            <div class="mission_div mission-right">
                <div class="mis-one">
                    <div class="mis-left">
                        <img src="static/imag/6.jpg" alt=""/>
                    </div>
                    <div class="mis-right">
                        <h3>趣味导引</h3>
                        <p>这是一方情趣横生的小天地，知识在这里生动演绎；这是一个智慧四射的大课堂，灵感从其中勃然激发。精妙的文字，传神的漫画，让你在无限的向往中，步入知识的殿堂。</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="mis-one">
                    <div class="mis-left">
                        <img src="static/imag/8.jpg" alt=""/>
                    </div>
                    <div class="mis-right">
                        <h3>知识清单</h3>
                        <p>这是千万老教师的经验，这是无数成功者的累积。这是最系统的归纳，这是最科学的设计。将学科知识设计成习题，便于你在练习中实现对学科基本概念、基本知识的理解和记忆。实践证明，这是进行基础训练的最好方式。你要记死，不要死记</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="mis-one">
                    <div class="mis-left">
                        <img src="static/imag/7.jpg" alt=""/>
                    </div>
                    <div class="mis-right">
                        <h3>热点例析</h3>
                        <p>这是对教材精华的浓缩，这是对教材的精讲精析，这是点金拨雾的手指。当你透彻地掌握了教材知识，你就能以不变应万变，从容地面对每一次考试。</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<footer>
			<p></p>
				<div class="services">品质保障|七天无理由退换货|特色服务体验 </div>
		</footer>
		<script type="text/javascript" src="static/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="static/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="static/js/jquery.simplesidebar.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.sidebar').simpleSidebar({
					settings: {
						opener: '#open-sb',
						wrapper: '.wrapper',
						animation: {
							duration: 500,
							easing: 'easeOutQuint'
						}
					},
					sidebar: {
						align: 'left',
						width: 200,
						closingLinks: 'a',
					}
				});
			});
		</script>

		<script type="text/javascript">
			$(function() {
				$(".subNav").click(function() {
					$(this).next(".navContent").slideToggle(500);
				})
			})
		</script>
		<script type="text/javascript" src="static/js/pageSwitch.min.js"></script>
		<script>
			$("#container").PageSwitch({
				direction: 'horizontal',
				easing: 'ease-in',
				duration: 1000,
				autoPlay: true,
				loop: 'false'
			});
		</script>
	</body>

</html>