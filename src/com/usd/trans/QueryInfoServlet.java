package com.usd.trans;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.usd.bean.LoginBean3;
import com.usd.bean.UserCommBean;
import com.usd.service.QueryAjaxImpl;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class QueryInfoServlet
 */
@WebServlet("/QueryInfoServlet")
public class QueryInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QueryInfoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=utf-8");
		// 查询匹配到的数据存入集合当中
		List<LoginBean3> list = Myutils.getInstence(QueryAjaxImpl.class).queryAjax(new LoginBean3());
		// 借助第三方的工具将集合转换为json字符串【alibaba的fastjson插件包】
		// 将查询结果集list转换为json字符串
		String strJson = JSON.toJSONStringWithDateFormat(list, "yyyy-MM-dd HH:mm:ss");
		PrintWriter out = null;
		if (strJson.length() >1) {
			// 将数据返回请求端回调函数中
			out = response.getWriter();
			out.print(strJson);
			out.flush();
			out.close();
		} else {
			out = response.getWriter();
			out.print("抱歉查无此人");
			out.flush();
			out.close();
		}
	}

}
