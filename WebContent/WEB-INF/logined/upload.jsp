<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>发布物品</title>
    <link rel="stylesheet" href="static/upload/css/register.css" />
    <link rel="stylesheet" href="static/css/succulent.css" />
    <link rel="stylesheet" href="static/css/test1.css" />
    <script type="text/javascript" src="static/jquery/jquery.min.js" ></script>
    <script type="text/javascript">
	//使用jquery进行ajax处理
	//var form = new FormData(document.getElementById("mainForm"));
	function publish_jquery() {
		$
				.ajax({
					url : "PublishServlet",//请求servlet映射地址
					type : "POST",//请求类型
					contentType : "multipart/form-data",
					data: new FormData($('#mainform')[0]), // 发送的数据 使用serialize方法替代手动拼写数据串,序列化表格内容为字符串的异步请求
					processData: false, 
					contentType: false,
					success : function(data, textStatus, jqhr) {//由服务器返回的数据，描述请求结果，
						//$("#userMsg").html(data + "----" + textStatus);
						alert("发布成功，pice!!");
						if (data=="1"){
	  						window.location.href = "turnShelf.jsp";
	  						}else{
	  							alert("发布失败，再点一次那个按钮，快！！！")
	  							
	  						} 

					},
					error : function(jqhr) {
						alert("发布失败，请与甘川华老板联系");
					}
				});
	}
    </script>
</head>
<body>
	<header>
			<div class="bar">
				<form>
					<input type="text" placeholder="请输入您要搜索的内容...">
					<button type="submit" id="button1">搜索</button>
				</form>
			</div>
			<div class="a">
			</div>
		</header>
	<canvas id="canvas" >	
	</canvas>	
<div>

<form class="contact_form gallery" action="PublishServlet" enctype="multipart/form-data" id="mainform" method="post" >
    <ul>
        <li class="usually">
             <h1>发布物品</h1>
        </li>
        <li class="usually">
            <span>物品名称:</span>
            <input type="text" id="name" name="commodity" required />
        </li>
        <li class="usually">
            <span>联系邮箱:</span>
            <input type="email"  name="email" placeholder="javin@example.com" required />
        </li>
        <li class ="usually">
        	<span>选择图片:</span>
        	<input type="file" name="picture"/>
        </li>
        <li class="usually">
            <span>所在城市:</span>
            <select name="city">
                <option>-请选择-</option>
    	        <option selected="selected">北京</option>
    	        <option>上海</option>
    	        <option>广州</option>
    	        <option>深圳</option>
    	        <option>上饶</option>
    	        <option>新余</option>
    	        <option>九江</option>
    	        <option>宜春</option>
    	        <option>赣州</option>
    	        <option>景德镇</option>
    	        <option>吉安</option>
    	    </select>
        </li>
        <li class="special">
            <span >新旧程度:</span>
            <input type="radio" name="newold" id="male"  value="99" checked/>
            <label for="male">99</label>
            <input type="radio" name="newold" id="female" value="95"/>
            <label for="female">95</label>
            <input type="radio" name="newold" id="female" value="90"/>
            <label for="female">90</label>
            <input type="radio" name="newold" id="female"value="85" />
            <label for="female">85</label>
        </li>
        <li class="usually">
            <span>发布价格:</span>
            <input type="number" name="price" required/>&nbsp;&nbsp;元
        </li>
        <li class="special">
            <span >受损部位:</span>
            <input type="checkbox" id="football" name="position" value="无法使用"/>
            <label for="football">无法使用</label>
            <input type="checkbox" id="basketball" name="position" value="还能坚持" />
            <label for="basketball">还能坚持</label>
            <input type="checkbox" id="swim" name="position" value="必须使用"/>
             <label for="swim">必须能用</label>
            <input type="checkbox" id="sing" name="position" value="无敌正常"/>
            <label for="sing">无敌正常</label>
        </li>
        <li class="usually">
            <span>详细介绍:</span>
            <textarea rows="10" cols="200" name="introduce" placeholder="Please enter your message"
        class="message" required></textarea>
        </li>
        <li >
            <button class="submit" type="button" onclick="publish_jquery()">立即发布</button>
        </li>
    </ul>
</form>
    </div>
    <div class="toolbar">
			<div id="open-sb" class="menu-button menu-left"></div>
		</div>
    <section class="sidebar">
			<div class="subNav">
				<h5>菜单</h5></div>
			<hr />
			<div class="subNav">
				<a href="${basePath}JumpServlet?jump=WEB-INF/logined/indexMain"><h6>主页</h6></a></div>
			<div class="subNav">
				<h6>新品推荐</h6></div>
			<ul class="navContent">
				<li>
					<a href="#">当季新品</a>
				</li>
			</ul>
			<div class="subNav">
				<h6>书</h6></div>
			<div class="subNav">
				<h6>服装</h6></div>
			<div class="subNav">
				<h6>手机</h6></div>
			<ul class="navContent">
				<li>
					<a href="#">安卓</a>
				</li>
				<li>
					<a href="#">IPHONE</a>
				</li>
				<li>
					<a href="#">其他</a>
				</li>
			</ul>
			<div class="subNav">
				<h6>车</h6></div>
			<ul class="navContent">
				<li>
					<a href="#">大巴</a>
				</li>
				<li>
					<a href="#">轿车</a>
				</li>
				<li>
					<a href="#">摩托</a>
				</li>
			</ul>
			<div class="subNav">
				<h6>代金卷</h6></div>
			<div class="subNav">
				<h6>关于我们</h6></div>
			<ul class="navContent">
				<li>
					<a href="#">使命宣言</a>
				</li>
			</ul>
			<div class="subNav">
				<h6>帮助中心</h6></div>
			<ul class="navContent">
				<li>
					<a href="#">常见问题</a>
				</li>
				<li>
					<a href="#">联系我们</a>
				</li>
				<li>
					<a href="#">使用条款</a>
				</li>
				<li>
					<a href="#">隐私权政策</a>
				</li>
			</ul>
		</section>
</body>
<script>
var canvas = document.querySelector('#canvas');
var ctx = canvas.getContext("2d");
var starlist = [];
function init(){
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
}
init();
window.onresize = init;

canvas.addEventListener('mousemove',function(e){
	starlist.push(new Star(e.offsetX,e.offsetY));
	console.log(starlist)
})

function random(min,max){
	return Math.floor((max-min)*Math.random()+ min);
}

function Star(x,y){
	this.x = x;
	this.y = y;
	this.vx = (Math.random()-0.5)*3;
	this.vy = (Math.random()-0.5)*3;
	this.color = 'rgb('+random(0,256)+','+random(0,256)+','+random(0,256)+')';
	this.a = 1;
	console.log(this.color);
	this.draw();
}
Star.prototype={
	draw:function(){
		ctx.beginPath();
		ctx.fillStyle = this.color;
		ctx.globalCompositeOperation='lighter'
		ctx.globalAlpha= this.a;
		ctx.arc(this.x,this.y,10,0,Math.PI*2,false);
		ctx.fill();
		this.updata();
	},
	   updata(){
		this.x+=this.vx;
		this.y+=this.vy;
		this.a*=0.98;
	}
}
console.log(new Star(150,200));
function render(){
	ctx.clearRect(0,0,canvas.width,canvas.height)
	
	 starlist.forEach((item,i)=>{
		item.draw();
		if(item.a<0.05){
			starlist.splice(i,1);
		}
	})
	
	requestAnimationFrame(render);
}
render();	
</script>
<script type="text/javascript" src="static/jquery/jquery.min.js" ></script>
<script type="text/javascript" src="static/js/jquery-ui.min.js" ></script>
<script type="text/javascript" src="static/js/jquery.simplesidebar.js" ></script>
<script>
$(document).ready(function() {
				$('.sidebar').simpleSidebar({
					settings: {
						opener: '#open-sb',
						wrapper: '.wrapper',
						animation: {
							duration: 500,
							easing: 'easeOutQuint'
						}
					},
					sidebar: {
						align: 'left',
						width: 200,
						closingLinks: 'a',
					}
				});
			});
		</script>
		<script type="text/javascript">
			$(function() {
				$(".subNav").click(function() {
					$(this).next(".navContent").slideToggle(500);
				})
			})
		</script>
		<script type="text/javascript" src="static/js/pageSwitch.min.js"></script>
		<script>
			$("#container").PageSwitch({
				direction: 'horizontal',
				easing: 'ease-in',
				duration: 1000,
				autoPlay: true,
				loop: 'false'
			});
		</script>
</div>
</html>