package com.usd.bean;

public class ShoppingcarComBean {
	private String iphone;
	private String commodity;
	private String picture;
	private Integer price;
	private Integer commid;
	private Integer shopid;
	
	public ShoppingcarComBean() {
		super();
	}
	public ShoppingcarComBean(String iphone, String commodity, String picture, Integer price, Integer commid,
			Integer shopid) {
		super();
		this.iphone = iphone;
		this.commodity = commodity;
		this.picture = picture;
		this.price = price;
		this.commid = commid;
		this.shopid = shopid;
	}
	public String getIphone() {
		return iphone;
	}
	public void setIphone(String iphone) {
		this.iphone = iphone;
	}
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getCommid() {
		return commid;
	}
	public void setCommid(Integer commid) {
		this.commid = commid;
	}
	public Integer getShopid() {
		return shopid;
	}
	public void setShopid(Integer shopid) {
		this.shopid = shopid;
	}
	@Override
	public String toString() {
		return "ShoppingcarComBean [iphone=" + iphone + ", commodity=" + commodity + ", picture=" + picture + ", price="
				+ price + ", commid=" + commid + ", shopid=" + shopid + "]";
	}
	

}