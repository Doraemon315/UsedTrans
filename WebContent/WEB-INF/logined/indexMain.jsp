<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<%
String beanUser=(String)request.getSession().getAttribute("beanUser");
if (beanUser==null){
	request.getRequestDispatcher("/turn.jsp").forward(request, response);
	return;
}
%>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="static/css/succulent.css">
		<link rel="stylesheet" href="static/css/zzsc.css" />
		<link rel="stylesheet" href="static/src/jquery.skidder.css" />
		<link rel="stylesheet" href="static/css/rightflow.css" />
		<script type="text/javascript">
		//使用jquery方ajax查询
		function doAjaxQuery() {
			$.ajax({
				url : "QueryServlet",
				type : "POST",
				//data : $("#userName").serialize(),//获取ID属性值序列化
				dataType : "json", // 响应发挥的数据类型  
				success : function(data, Statstus) {
					//分页处理
					//拼凑html内容
					var strHtml = "";
					//data：要循环遍历的数据集合，index：循环中的下标，eachVal循环中index下标存放的值 
					$
							.each(
									data,
									function(index, eachVal) {
										strHtml += "<div class='view'>";
										strHtml += "<img src='";
										strHtml +=eachVal.picture;
										strHtml +="' width='200px' height='200px'  alt=''>"
										strHtml += "<div class='hover'>";
										strHtml += "<h3></h3>";
										strHtml += "<p><a href='${basePath}TurnPayServlet?commid="+eachVal.commid+"'>购买</a></p>";
										strHtml += "<p><a href='javascript:join_shoppingcar("+eachVal.commid+")'>加入购物车</a></p>";
										strHtml += "</div>";
										strHtml += "</div>";
									});
					$("#main").html(strHtml);
				},
				error : function(data) {
					
				}
			});
		}
		window.onload = doAjaxQuery;
		function join_shoppingcar(commid){
			 //使用jquery进行ajax处理	  		
		  				$.ajax({
		  					url : "JoinShoppingcarServle",//请求servlet映射地址
		  					type : "POST",//请求类型
		  					data:  "commid="+commid,
		  					success : function(data, textStatus, jqhr) {//由服务器返回的数据，描述请求结果，
		  						alert(data);
		  					},
		  					error : function(jqhr) {
		  						alert("赶快和强老板联系！！！！");
		  					}
		  				});
		  
		}
		</script>
		<title>二手商场</title>
	</head>

	<body>
		<header>
			<div class="logo"><p>Let</p></div>
			<div class="bar">
				<form>
					<input type="text" placeholder="请输入您要搜索的内容...">
					<button type="submit" id="button1">搜索</button>
				</form>
			</div>
			<span class="persons"><img src="${beanTpass}" /></span>
			<span class="usernamed">${beanUser}</span>
		</header>
		<div class="slideshow" style="height: 0; margin-top:15px;overflow: hidden">
			<div class="slide"><img src="static/img/1.jpg"></div>
			<div class="slide"><img src="static/img/2.jpg"></div>
			<div class="slide"><img src="static/img/3.jpg"></div>
			<div class="slide"><img src="static/img/4.jpg"></div>
			<div class="slide"><img src="static/img/5.jpg"></div>
		</div>
		<div class="htmleaf-container">
			<nav class="animenu">
				<button class="animenu__toggle">
		<span class="animenu__toggle__bar"></span>
		<span class="animenu__toggle__bar"></span>
		<span class="animenu__toggle__bar"></span>
	  </button>
				<ul class="animenu__nav">
					<li>
						<a href="${basePath}JumpServlet?jump=WEB-INF/logined/text2">租赁</a>
					</li>
					<li>
						<a href="#">电子产品</a>
						<ul class="animenu__nav__child">
							<li>
								<a href="">手机</a>
							</li>
							<li>
								<a href="">电脑</a>
							</li>
							<li>
								<a href="">其他</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">体育用品</a>
						<ul class="animenu__nav__child">
							<li>
								<a href="">球类</a>
							</li>
							<li>
								<a href="">健身器材</a>
							</li>
							<li>
								<a href="">其他</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">旧书</a>
					</li>
					<li>
						<a href="#">影像制品</a>
					</li>
				</ul>
			</nav>
		</div>
		<div class="img">
			<img src="static/images/illust_63373510_20180526_013839.jpg" width="100%" />
		</div>
		<div class="border-radius">最新商品</div>
		<div class="main" id="main">
			<div class="view">
				<img src="static/images/1.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/7.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/3.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/4.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/5.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcarl">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/6.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/2.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/1.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/2.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/3.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/4.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/5.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>
			<div class="view">
				<img src="static/images/6.jpg" alt="">
				<div class="hover">
					<h3></h3>
					<p><a href="#">购买</a></p>
					<p><a href="${basePath}JumpServlet?jump=WEB-INF/logined/buyingcar">加入购物车</a></p>
				</div>
			</div>	
		</div>
		<section>
			<p>
			</p>
		</section>
		<footer>
			<p></p>
			<div class="services">品质保障|七天无理由退换货|特色服务体验 </div>
		</footer>
		<!--***********右侧悬浮条*********-->
		<div class="toolbars">
	    	<ul>
	    		<li class="xyvip">
	    			<i></i>
	    			<a class="toolbars-ico" href="#"></a>
	    			<a class="flex" href="#">新余会员</a>
	    		</li>
	    		<li class="cart">
	    			<a class="toolbars-ico" href="#"></a>
	    			<a class="flex" href="${basePath}JumpServlet?jump=WEB-INF/logined/shoppingcar">购物车</a>
	    		</li>
	    		<li class="follow">
	    			<a class="toolbars-ico" href="#"></a>
	    			<a class="flex" href="#">我的关注</a>
	    		</li>
	    		<li class="message">
	    			<a class="toolbars-ico" href="#"></a>
	    			<a class="flex" href="#">我的消息</a>
	    		</li>
	    		<li class="jimi">
	    			<a class="toolbars-ico" href="#"></a>
	    			<a class="flex" href="#">咨询JIMI</a>
	    		</li>
	    		<li class="feedback">
	    			<a class="toolbars-ico" href="#"></a>
	    			<a class="flex" href="#">反馈</a>
	    		</li>
	    	</ul>
	    	<div class="toolbars-footer">
	    		<a class="toolbars-ico" href="#"></a>
	    		<a class="flex" href="#">顶部</a>
	    	</div>	    	
	    </div>
	    <script>
	     // 原始版
	     $(function(){
	         $('.toolbars-footer a').click(function(){
	             $('html , body').animate({scrollTop: 0},'slow');
	         });
	     });
	     </script>
		<!--***************************-->
		<script type="text/javascript" src="static/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="static/src/jquery.skidder.js"></script>
		<script type="text/javascript" src="static/js/imagesloaded.js"></script>
		<script type="text/javascript" src="static/js/smartresize.js"></script>
		<script src="static/src/jquery.skidder.js"></script>
		<script type="text/javascript">
			$('.slideshow').each(function() {
				var $slideshow = $(this);    
				$slideshow.imagesLoaded(function() {
					$slideshow.skidder({
						slideClass: '.slide',
						animationType: 'css',
						scaleSlides: true,
						maxWidth: 1300,
						maxHeight: 500,
						paging: true,
						autoPaging: true,
						pagingWrapper: ".skidder-pager",
						pagingElement: ".skidder-pager-dot",
						swiping: true,
						leftaligned: false,
						cycle: true,
						jumpback: false,
						speed: 400,
						autoplay: false,
						autoplayResume: false,
						interval: 4000,
						transition: "slide",
						afterSliding: function() {},
						afterInit: function() {}
					});
				});
			});
			$(window).smartresize(function() {
				$('.slideshow').skidder('resize');
			});
		</script>
		<script type="text/javascript">
			(function() {
				var animenuToggle = document.querySelector('.animenu__toggle'),
					animenuNav = document.querySelector('.animenu__nav'),
					hasClass = function(elem, className) {
						return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
					},
					toggleClass = function(elem, className) {
						var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
						if(hasClass(elem, className)) {
							while(newClass.indexOf(' ' + className + ' ') >= 0) {
								newClass = newClass.replace(' ' + className + ' ', ' ');
							}
							elem.className = newClass.replace(/^\s+|\s+$/g, '');
						} else {
							elem.className += ' ' + className;
						}
					},
					animenuToggleNav = function() {
						toggleClass(animenuToggle, "animenu__toggle--active");
						toggleClass(animenuNav, "animenu__nav--open");
					}

				if(!animenuToggle.addEventListener) {
					animenuToggle.attachEvent("onclick", animenuToggleNav);
				} else {
					animenuToggle.addEventListener('click', animenuToggleNav);
				}
			})()
		</script>
	</body>

</html>
