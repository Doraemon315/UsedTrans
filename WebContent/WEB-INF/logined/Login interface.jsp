<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link rel="stylesheet" href="static/landing/css/GalMenu.css" />
		<link rel="stylesheet" href="static/landing/css/login.css" />
		<script type="text/javascript" src="static/jquery/jquery.min.js" ></script>
		<script type="text/javascript">
		var items = document.querySelectorAll('.menuItem');
	      for (var i = 0,
	      l = items.length; i < l; i++) {
	        items[i].style.left = (50 - 35 * Math.cos( - 0.5 * Math.PI - 2 * (1 / l) * i * Math.PI)).toFixed(4) + "%";
	        items[i].style.top = (50 + 35 * Math.sin( - 0.5 * Math.PI - 2 * (1 / l) * i * Math.PI)).toFixed(4) + "%"
	      }
	    //使用jquery进行ajax处理
	  	function save_jquery() {
	  		$
	  				.ajax({
	  					url : "QueryServlet2",//请求servlet映射地址
	  					type : "POST",//请求类型
	  					data:  $('#mainForm').serialize(),// 发送的数据 使用serialize方法替代手动拼写数据串,序列化表格内容为字符串的异步请求
	  					success : function(data, textStatus, jqhr) {//由服务器返回的数据，描述请求结果，
	  						if (data=="1"){
	  						window.location.href = "turnMain.jsp";
	  						}else{
	  							alert("账号密码错误，去注册吧")
	  							window.location.href = "turnZhuce.jsp";
	  						} 

	  					},
	  					error : function(jqhr) {
	  						alert("已经点过一次了，等一下嘛！！！！");
	  					}
	  				});
	  	}
	   
		
		</script>
	</head>
	<body>
		<form action="QueryServlet2" id="mainForm"  method="post">
			<div class="border-radius"></div>
			<input type="text" name="iphone"id="iphone" placeholder="手机号" />
			<input type="password" name="pass" id="pass" placeholder="密码" />
			<input type="button" name="submit" value="登陆" class="btn" onclick="save_jquery()"/>
		</form>
		<body>
	<div class="GalMenu GalDropDown">
	      <div class="circle" id="gal">
	        <div class="ring">
	          <a href="index.html" title="" class="menuItem">首页</a>
	          <a href="#" title="" class="menuItem">忘记密码</a>
	          <a href="#" title="" class="menuItem">注册账号</a>
	          <a href="#" title="" class="menuItem">在线客服</a>
	          <a href="#" title="" class="menuItem">关于我们</a>
	          <a href="#" title="" class="menuItem">留言</a></div>
	      </div>
	</div>
	<div id="overlay" style="opacity: 1; cursor: pointer;"></div>

	<script type="text/javascript" src="static/jquery/jquery.min.js" ></script>
	<script>window.jQuery || document.write('<script src="static/js/jquery.min.js"><\/script>')</script>
	<script type="text/javascript" src="static/landing/js/GalMenu.js" ></script>
    <script type="text/javascript">
    	$(document).ready(function() {
        $('body').GalMenu({
          'menu': 'GalDropDown'
        })
      });
    
    	</script>
	</body>
</html>
