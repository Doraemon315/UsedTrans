package com.usd.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.usd.bean.Commodity;
import com.usd.bean.LoginBean3;
import com.usd.bean.ShoppingcarComBean;
import com.usd.bean.UserCommBean;
import com.usd.db.DbProvider;
import com.usd.serviceinter.IQueryAjax;
import com.usd.utils.Myutils;

public class QueryAjaxImpl implements IQueryAjax {

	@Override
	public List<LoginBean3> queryAjax(LoginBean3 bean) {
		List<LoginBean3> beanList = null;
		// TODO Auto-generated method stub
		// 初始化查询语 句
		StringBuilder strSql = new StringBuilder("select * from info where 1=1 ");
		// 查询条件参数的集 合
		List<Object> listPra = new ArrayList();
		// 提取存进bean中的request数据的集合
		String userName = bean.getUserName();
		// 进行姓名模糊查询
		if (userName != null && !userName.equals("")) {
			// 将sql筛选跳进append进sql语句
			strSql.append(" and user like ? ");
			// 添加查询参数进集合当中
			listPra.add("%" + userName + "%");
		}

		String userSex = bean.getSex();
		// 进行性别模糊查询
		if (userSex != null && !userSex.equals("")) {
			// 将sql筛选跳进append进sql语句
			strSql.append(" and Sex like ? ");
			// 添加查询参数进集合当中
			listPra.add("%" + userSex + "%");
		}

		// 将拼接的sql语句，和将装有数据的bean载进list中的参数，装有占位符参数的集合转为数组
		// 将查询到的结果传回调用者

		beanList = Myutils.getInstence(DbProvider.class).query(strSql.toString(),
				new BeanListHandler<>(LoginBean3.class), listPra.toArray());
		return beanList;
	}

	@Override
	public List<Commodity> queryAjax(String beanIphone) {
		// TODO Auto-generated method stub
		List<Commodity> beanList = null;
		// TODO Auto-generated method stub
		// 初始化查询语 句
		String strSql = "select * from Commodity where iphone not in ("+beanIphone+")";
		// 将查询到的结果传回调用者

		beanList = Myutils.getInstence(DbProvider.class).query(strSql.toString(),
				new BeanListHandler<>(Commodity.class));
		return beanList;
	}
	public List<Commodity> queryPercomm(HttpServletRequest request) {
		// TODO Auto-generated method stub
		List<Commodity> beanList = null;
		// 获取当前登录的账号信息
		String beanIphone=(String)request.getSession().getAttribute("beanIphone");
		// 初始化查询语 句
		String strSql="select * from Commodity where 1=1 and iphone="+beanIphone;
		// 将查询到的结果传回调用者
		beanList = Myutils.getInstence(DbProvider.class).query(strSql,
				new BeanListHandler<>(Commodity.class));
		return beanList;
	}

	@Override
	public List<Commodity> queryPerson(HttpServletRequest request) {
		// TODO Auto-generated method stub
		List<Commodity> beanList = null;
		// 获得已上架物品的外键
		String beanIphone = (String) request.getSession().getAttribute("beanIphone");
		// TODO Auto-generated method stub
		// 初始化查询语 句
		String strSql = "select * from Commodity where Iphone =" + beanIphone;
		// 将查询到的结果传回调用者

		beanList = Myutils.getInstence(DbProvider.class).query(strSql.toString(),
				new BeanListHandler<>(Commodity.class));
		return beanList;
	}

	@Override
	public List<UserCommBean> queryUserComm(HttpServletRequest request) {
		// TODO Auto-generated method stub

		List<UserCommBean> beanList = null;
		// 获得每个用户上架的物品
		// 初始化获得每个用户上架的物品查询语句
		String strSql = "select c.commodity,c.price,c.picture,i.iphone,c.introduce,c.commid " + " from commodity c,info i"
				+ " where c.iphone=i.iphone";
		beanList = Myutils.getInstence(DbProvider.class).query(strSql, new BeanListHandler<>(UserCommBean.class));
		return beanList;
	}

	public List<UserCommBean> queryPersonComm(HttpServletRequest request) {
		// TODO Auto-generated method stub

		List<UserCommBean> beanList = null;
		String beanIphone=(String) request.getSession().getAttribute("beanIphone");
		// 获得每个用户上架的物品
		// 初始化获得每个用户上架的物品查询语句
		String strSql = "select c.commodity,c.price,c.picture,i.iphone,c.introduce" + " from commodity c,info i"
				+ " where c.iphone=i.iphone and i.iphone="+beanIphone;
		beanList = Myutils.getInstence(DbProvider.class).query(strSql, new BeanListHandler<>(UserCommBean.class));
		return beanList;
	}
	public List<Commodity> queryPayComm(String commid) {
		// TODO Auto-generated method stub
		List<Commodity> beanList = null;
		// TODO Auto-generated method stub
		// 初始化查询语 句
		String strSql = "select * from Commodity where commid =" + commid;
		// 将查询到的结果传回调用者

		beanList = Myutils.getInstence(DbProvider.class).query(strSql,
				new BeanListHandler<>(Commodity.class));
		return beanList;
	}
	public List<ShoppingcarComBean> queryShoppingCar(String beanIphone) {
		// TODO Auto-generated method stub
		List<ShoppingcarComBean> beanList = null;
		// TODO Auto-generated method stub
		// 初始化查询语 句
		String strSql = "select * from shoppingcar where iphone =" + beanIphone;
		// 将查询到的结果传回调用者

		beanList = Myutils.getInstence(DbProvider.class).query(strSql,
				new BeanListHandler<>(ShoppingcarComBean.class));
		return beanList;
	}

}
