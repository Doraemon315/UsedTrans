package com.usd.trans;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usd.bean.Commodity;
import com.usd.service.QueryAjaxImpl;
import com.usd.service.VerLogin;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class JoinShoppingcarServle
 * 加购物车servlet
 * 
 */
@WebServlet("/JoinShoppingcarServle")
public class JoinShoppingcarServle extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public JoinShoppingcarServle() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 设置返回文本的编码格式
		response.setContentType("text/html;charset=utf-8");
		// 获得商品编号id
		String commid = request.getParameter("commid");
		String beanIphone =(String)request.getSession().getAttribute("beanIphone");
		List<Commodity> listBean = Myutils.getInstence(QueryAjaxImpl.class).queryPayComm(commid);
		//提取第一个bean对象存进bean对象，并且list集合中也只有一个对象
		Commodity commBean=null;
		for (Commodity commodity : listBean) {
			 commBean=commodity;
		}
		
		int saveLine=Myutils.getInstence(VerLogin.class).joinShoppingCar(commBean,beanIphone);
		PrintWriter out=response.getWriter();
		if(saveLine>0) {
			out.print("加入购物车成功哦亲！");
		}else {
			out.print("你的购物车满了");
		}
	}

}
