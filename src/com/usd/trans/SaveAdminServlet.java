package com.usd.trans;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usd.bean.AdminBean;
import com.usd.bean.LoginBean3;
import com.usd.service.VerLogin;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class SaveAdminServlet
 */
@WebServlet("/SaveAdminServlet")
@MultipartConfig(maxFileSize=1024*1024*5) // 计算的基本单位是字节（byte）限制上传文件大写为5m
public class SaveAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveAdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//设置返回文本的编码格式
		response.setContentType("text/html;charset=utf-8");
		//接收数据,将表单数据存入bean中
		AdminBean bean=Myutils.assignBean(request, AdminBean.class);
		//bean中数据保存至数据库中
		int saveLine=Myutils.getInstence(VerLogin.class).saveOrUpdateAdmin(bean);
		if(saveLine>0) {
			//请求转发至欢迎界面
			response.getWriter().print("添加管理员成功");
			//回调函数传回参数
		}else {
			response.getWriter().print("已有用户");
		}
	}

}
