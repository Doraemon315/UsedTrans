package com.usd.trans;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usd.bean.Commodity;
import com.usd.service.QueryAjaxImpl;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class TurnPayServlet
 */
@WebServlet("/TurnPayServlet")
public class TurnPayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TurnPayServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 从session中获取商品编号
		String commid = request.getParameter("commid");
		//从数据库中查找该commid的商品信息
	List <Commodity>beanList=Myutils.getInstence(QueryAjaxImpl.class).queryPayComm(commid);
	//从集合中获取商品信息
	for (Commodity comm : beanList) {
		//设置商品名称
		String beanComm=comm.getCommodity();
		request.getSession().setAttribute("beanComm", beanComm);
		//设置商品图片
		String beanPicture=comm.getPicture();
		request.getSession().setAttribute("beanPicture", beanPicture);
		//设置商品展示价格
		int beanPrice=comm.getPrice();
		request.getSession().setAttribute("beanPrice", beanPrice);
		//设置商品介绍简介
		String beanIntroduce=comm.getIntroduce();
		request.getSession().setAttribute("beanIntroduce", beanIntroduce);
		//设置商品id
		Integer beanCommid=comm.getCommid();
		request.getSession().setAttribute("beanCommid", beanCommid);
		
		
	}
		request.getRequestDispatcher("WEB-INF/logined/shopping.jsp").forward(request, response);
	}

}
