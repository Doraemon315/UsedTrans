package com.usd.bean;

public class UserCommBean {
	private String commodity;
	private String price;
	private String picture;
	private String iphone;
	private String introduce;
	private Integer commid;

	public UserCommBean() {
		super();
	}

	public UserCommBean(String commodity, String price, String picture, String iphone, String introduce,
			Integer commid) {
		super();
		this.commodity = commodity;
		this.price = price;
		this.picture = picture;
		this.iphone = iphone;
		this.introduce = introduce;
		this.commid = commid;
	}

	public String getCommodity() {
		return commodity;
	}

	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getIphone() {
		return iphone;
	}

	public void setIphone(String iphone) {
		this.iphone = iphone;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public Integer getCommid() {
		return commid;
	}

	public void setCommid(Integer commid) {
		this.commid = commid;
	}

	@Override
	public String toString() {
		return "UserCommBean [commodity=" + commodity + ", price=" + price + ", picture=" + picture + ", iphone="
				+ iphone + ", introduce=" + introduce + ", commid=" + commid + "]";
	}

}
