package com.usd.serviceinter;

import java.util.List;

import com.usd.bean.AdminBean;
import com.usd.bean.Commodity;
import com.usd.bean.LoginBean3;


/**
 * @author Ming 根据用户名和密码验证登录 返回结果集
 */
public interface BasicProviderInter {
	public List<LoginBean3> queryVer(LoginBean3 bean);
	public List<AdminBean> queryAdmin(AdminBean bean);
	public int saveOrUpdateAdminInf(LoginBean3 adminB);
	public int saveOrUpdateAdmin(AdminBean adminB);
	public int saveOrUpdateCommodity(Commodity comm);
	public int deleteUserInfo(String iphone);
	public int deleteCommInfo(String picture);
	
	
}
