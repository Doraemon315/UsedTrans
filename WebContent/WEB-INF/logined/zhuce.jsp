<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>用户注册</title>
		<link rel="stylesheet" type="text/css" href="static/css/succulent.css">
		<link rel="stylesheet" href="static/css/zzsc.css" />
		<link rel="stylesheet" href="static/src/jquery.skidder.css" />
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="static/layui/css/layui.css" />
		<link rel="stylesheet" href="static/layui/css/layui.mobile.css" />
		<script type="text/javascript" src="static/layui/lay/modules/form.js"></script>
		<script type="text/javascript">
		 //使用jquery进行ajax处理
	  	function zhuce_jquery() {
		$
				.ajax({
					url : "SaveServlet",//请求servlet映射地址
					type : "POST",//请求类型
					contentType : "multipart/form-data",
					data: new FormData($('#mainForm')[0]), // 发送的数据 使用serialize方法替代手动拼写数据串,序列化表格内容为字符串的异步请求
					processData: false, 
					contentType: false,
					success : function(data, textStatus, jqhr) {//由服务器返回的数据，描述请求结果，
						if (data=="1"){
	  						window.location.href = "turnLogin.jsp";
	  						}else{
	  							alert("已存在手机号，换张卡再来！！")
	  							
	  						} 

					},
					error : function(jqhr) {
						alert("发布失败，请与甘川华老板联系");
					}
				});
	}
		 </script>

		<script src="layui/layui.all.js"></script>
		 </head>
			<header>
				<div class="bar">
					<form>
						<input type="text" placeholder="请输入您要搜索的内容...">
						<button type="submit" id="button1">搜索</button>
					</form>
				</div>
				<div class="a">
					<a href="${basePath}JumpServlet?jump=WEB-INF/logined/Login interface">
						<button id="button1">登录</button>
					</a>
					<button type="button" id="button1">注册</button>
				</div>
			</header>

			<body>
				<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
					<legend>用户信息注册</legend>
				</fieldset>
				<div class="layui-main site-inline">
					<form class="layui-form" action="SaveServlet" id="mainForm" enctype="multipart/form-data" method="post">
						<div class="layui-form-item">
							<label class="layui-form-label">昵称</label>
							<div class="layui-input-inline">
								<input type="text" name="userName" lay-verify="title" autocomplete="off" placeholder="请输入昵称" class="layui-input">
							</div>
							<div class="layui-form-mid layui-word-aux">最少5个字符</div>
						</div>
						<div class="layui-form-item">
							<label class="layui-form-label">密码</label>
							<div class="layui-input-inline">
								<input type="password" name="pass" lay-verify="pass" placeholder="请输入密码" autocomplete="off" class="layui-input">
							</div>
							<div class="layui-form-mid layui-word-aux">请填写6到12位密码</div>
						</div>

						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label">手机</label>
								<div class="layui-input-inline">
									<input type="tel" name="iphone" lay-verify="required|phone" placeholder="+86" autocomplete="off" class="layui-input">
								</div>
							</div>
							<!-- <div class="layui-inline">
								<label class="layui-form-label">验证码</label>
								<div class="layui-input-inline">
									<input  class="layui-input">
								</div>
							</div> -->
							<div class="layui-form-item">
								<label class="layui-form-label">邮箱</label>
								<div class="layui-input-inline">
									<input type="text" name="email" lay-verify="email" placeholder="XXXX@XX.com" autocomplete="off" class="layui-input">
								</div>
							</div>
						</div>
						<div class="layui-form-item">
							<label class="layui-form-label">性别</label>
							<div class="layui-input-block">
								<input type="radio" name="sex" value="男" title="男">
								<div class="layui-unselect layui-form-radio ">
									<i class="layui-anim layui-icon layui-anim-scaleSpring"></i>
									<div>男</div>
								</div>
								<input type="radio" name="sex" value="女" title="女" checked="">
								<div class="layui-unselect layui-form-radio">
									<i class="layui-anim layui-icon layui-anim-scaleSpring"></i>
									<div>女</div>
								</div>
							</div>
						</div>
						<div class="layui-input-inline">
							<div class="layui-inline">
								<label class="layui-form-label">注册日期</label>
								<div class="layui-input-inline">
									<input type="text" name="birthday" id="date" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
								</div>
							</div>
						</div>
						<div class="layui-inline">
							<label class="layui-form-label">头像</label>
							<input type="file" class="layui-btn" name="tpass">
  								<i class="layui-icon">&#xe67c;</i>上传
						</div>
						<div class="layui-upload-list">
							<img  class="layui-upload-img" id="demo1">
							<p id="demoText"></p>
						</div>

						<div class="layui-form-item">
							<label class="layui-form-label">确认密码</label>
							<div class="layui-input-inline">
								<input type="password" name="apass" lay-verify="pass" placeholder="请输入密码" autocomplete="off" class="layui-input">
							</div>
							<div class="layui-form-mid layui-word-aux">请填写6到12位密码</div>
						</div>

						<div class="layui-form-item">
							<div class="layui-input-block">
								<button class="layui-btn" lay-submit="" lay-filter="demo1" type="button" onclick="zhuce_jquery()">注册</button>
								<button type="reset" class="layui-btn layui-btn-primary">重置</button>
							</div>
						</div>
					</form>

				</div>
			</body>
			<section>
				<p>
				</p>
			</section>
			<footer>
				<p></p>
				<div class="services">品质保障|七天无理由退换货|特色服务体验 </div>
			</footer>

			<script src="static/layui/lay/modules/form.js"></script>
			<script src="static/layui/layui.all.js"></script>
			<script type="text/javascript" src="static/jquery/jquery.js"></script>
			<script src="static/layui/lay/modules/upload.js"></script>
			<script>
				layui.use('form', function() {
					var form = layui.form;
					form.render();
				});
				layui.use(['form', 'layedit', 'laydate'], function() {
					var form = layui.form,
						layer = layui.layer,
						layedit = layui.layedit,
						laydate = layui.laydate;

					//日期
					laydate.render({
						elem: '#date'
					});
					laydate.render({
						elem: '#date1'
					});

					//创建一个编辑器
					var editIndex = layedit.build('LAY_demo_editor');

					//自定义验证规则
					form.verify({
						title: function(value) {
							if(value.length < 5) {
								return '用户名至少得5个字符';
							}
						},
						pass: [/(.+){6,12}$/, '密码必须6到12位'],
						content: function(value) {
							layedit.sync(editIndex);
						}
					});
					//监听提交
				/* 	form.on('submit(demo1)', function(data) {
						layer.alert(JSON.stringify(data.field), {
							title: '最终的提交信息'
						})
						return false;
					}); */
				});
				layui.use('upload', function() {
					var $ = layui.jquery,
						upload = layui.upload;

					//普通图片上传
					var uploadInst = upload.render({
						elem: 'tpass',
						url: 'SaveServlet',
						before: function(obj) {
							//预读本地文件示例，不支持ie8
							obj.preview(function(index, file, result) {
								$('#demo1').attr('src', result); //图片链接（base64）
							});
						},
						done: function(res) {
							//如果上传失败
							if(res.code > 0) {
								return layer.msg('上传失败');
							}
							//上传成功
						},
						error: function() {
							//演示失败状态，并实现重传
							var demoText = $('#demoText');
							demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
							demoText.find('.demo-reload').on('click', function() {
								uploadInst.upload();
							});
						}
					});

				});
			</script>

</html>