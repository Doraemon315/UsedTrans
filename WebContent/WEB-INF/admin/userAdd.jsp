<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!--引入bootstrap样式文件--->
<link rel="stylesheet" type="text/css"
	href="static/bootstrap-3.3.7-dist/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="static/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />

<link
	href="static/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" media="screen">
<style>
body {
	margin: 0px;
	padding: 0px;
}

#mainBody {
	width: 95%;
	margin: 0px auto;
}
</style>
<!--引入jquery-->
<script type="text/javascript"
	src="static/js/jquery-1.12.4/jquery-1.12.4.min.js"></script>
<!--引入bootstrap脚本--->
<script type="text/javascript"
	src="static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="static/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js"
	charset="UTF-8"></script>
<script type="text/javascript"
	src="static/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js"
	charset="UTF-8"></script>
<script type="text/javascript">
	$(function() {
		$(".form_datetime").datetimepicker({
			format : "yyyy-mm-dd hh:ii",
			autoclose : true,
			todayBtn : true,
			pickerPosition : "bottom-left"
		});

	})
	//使用jquery进行ajax处理
	function save_jquery() {
		$
				.ajax({
					url : "SaveAdminServlet",//请求servlet映射地址
					type : "POST",//请求类型
					contentType : "multipart/form-data",
					data: new FormData($('#mainForm')[0]), // 发送的数据 使用serialize方法替代手动拼写数据串,序列化表格内容为字符串的异步请求
					processData: false, 
					contentType: false,
					success : function(data, textStatus, jqhr) {//由服务器返回的数据，描述请求结果，
						$("#userMsg").html(data + "----" + textStatus);
						console.info(jqhr);

					},
					error : function(jqhr) {
						alert("意想不到的错误，请联系伟大的郑老板");
					}
				});
	}
</script>
</head>
<body>
	<div id="mainBody">
		<form class="form-horizontal" id="mainForm" action="SaveAdminServlet"
			enctype="multipart/form-data" method="post">
			<div class="form-group" style="margin-top: 10px;">
				<label class="col-sm-2 control-label">姓名</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="adminName"
						name="adminName" placeholder="请输入用户名">
				</div>

				<!-- <label class="col-sm-2 control-label">地址</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="userAdress"
						name="userAdress" placeholder="请输入地址">
				</div> -->

			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">密码</label>
				<div class="col-sm-3">
					<input type="password" class="form-control" id="adminPasd"
						name="adminPasd" placeholder="请输入密码">
				</div>
				<label class="col-sm-2 control-label">性别</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="adminSex" name="adminSex"
						placeholder="请输入性别">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">注册日期</label>
				<div class="col-sm-3">
					<input type="date" class="form-control" id="adminDate"
						name="adminDate" placeholder="请输入联系电话">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">头像</label>
				<div class="col-sm-3">
					<input type="file" class="form-control" id="adminUrl" name="adminUrl">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="button" class="btn btn-default"
						onclick="save_jquery()">保存</button>
					<label class="control-label" id="userMsg"></label>
				</div>
			</div>
		</form>
	</div>
</body>
</html>