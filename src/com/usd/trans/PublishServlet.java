package com.usd.trans;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usd.bean.Commodity;
import com.usd.service.VerLogin;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class PublishServlet
 */
@WebServlet("/PublishServlet")
@MultipartConfig(maxFileSize=1024*1024*5) // 计算的基本单位是字节（byte）
public class PublishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PublishServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//设置返回文本的编码格式
		response.setContentType("text/html;charset=utf-8");
		//接收数据,将表单数据存入bean中
		System.out.println(request.getRequestURL().toString());
		Commodity bean=Myutils.assignBean(request, Commodity.class);
		String beanIphone=(String) request.getSession().getAttribute("beanIphone");
		bean.setIphone(beanIphone);
		//bean.setIphone("15079197582");
		//bean中数据保存至数据库中
		int saveLine=Myutils.getInstence(VerLogin.class).saveOrUpdateCommodity(bean);
		if(saveLine>0) {
			response.getWriter().print("1");
		}else {
			response.getWriter().print("2");
		}
	}

}
