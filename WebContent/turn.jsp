<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		//获得项目应用名称的相对路径
		String path = request.getContextPath();
		//获得本项目的地址(例如: http://localhost:8080/MyApp/)赋值给basePath变量 
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
				+ path + "/";
		//将 "项目路径basePath" 放入pageContext中，待以后用EL表达式读出。 
		request.getSession().setAttribute("basePath", basePath);
		// 注意：静态资源尽可能的不要放在安全目录下，因为web-info安全目录只能通过转发访问；
		request.getRequestDispatcher("WEB-INF/logined/index.jsp").forward(request, response);
	%>
</body>
</html>