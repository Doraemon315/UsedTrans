package com.usd.bean;

public class Commodity {
	private Integer commid;
	private String commodity;
	private String iphone;
	private String picture;
	private String city;
	private String newold;
	private String position;
	private String introduce;
	private int price;

	public Commodity() {
		super();
	}

	public Commodity(Integer commid, String commodity, String iphone, String picture, String city, String newold,
			String position, String introduce, int price) {
		super();
		this.commid = commid;
		this.commodity = commodity;
		this.iphone = iphone;
		this.picture = picture;
		this.city = city;
		this.newold = newold;
		this.position = position;
		this.introduce = introduce;
		this.price = price;
	}

	public Integer getCommid() {
		return commid;
	}

	public void setCommid(Integer commid) {
		this.commid = commid;
	}

	public String getCommodity() {
		return commodity;
	}

	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}

	public String getIphone() {
		return iphone;
	}

	public void setIphone(String iphone) {
		this.iphone = iphone;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getNewold() {
		return newold;
	}

	public void setNewold(String newold) {
		this.newold = newold;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Commodity [commid=" + commid + ", commodity=" + commodity + ", iphone=" + iphone + ", picture="
				+ picture + ", city=" + city + ", newold=" + newold + ", position=" + position + ", introduce="
				+ introduce + ", price=" + price + "]";
	}

}
