package com.usd.bean;

import java.util.Date;

public class LoginBean3 {
	private String iphone;
	private String userName;
	private String tpass;
	private String pass;
	private String sex;
	private Date birthday;

	public LoginBean3() {
		super();
	}

	public LoginBean3(String iphone, String userName, String tpass, String pass, String sex, Date birthday) {
		super();
		this.iphone = iphone;
		this.userName = userName;
		this.tpass = tpass;
		this.pass = pass;
		this.sex = sex;
		this.birthday = birthday;
	}

	public String getIphone() {
		return iphone;
	}

	public void setIphone(String iphone) {
		this.iphone = iphone;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTpass() {
		return tpass;
	}

	public void setTpass(String tpass) {
		this.tpass = tpass;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Override
	public String toString() {
		return "LoginBean3 [iphone=" + iphone + ", user=" + userName + ", tpass=" + tpass + ", pass=" + pass + ", sex="
				+ sex + ", birthday=" + birthday + "]";
	}

}
