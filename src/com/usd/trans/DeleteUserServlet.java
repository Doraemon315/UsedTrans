package com.usd.trans;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usd.bean.LoginBean3;
import com.usd.service.VerLogin;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class DeleteUserServlet
 */
@WebServlet("/DeleteUserServlet")
public class DeleteUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * 删除用户在表中的信息，无法在商城中登录
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//设置返回文本的编码格式
		response.setContentType("text/html;charset=utf-8");
		//从request中获取数据
		String userId=request.getParameter("iphone");
		//bean中数据保存至数据库中
		int deleteLine=Myutils.getInstence(VerLogin.class).deleteUserInfo(userId);
		if(deleteLine>0) {
			//请求转发至欢迎界面
			response.getWriter().print(" 此人已废");
			//回调函数传回参数
		}else {
			response.getWriter().print(" 删除失败");
		}
	}

}
