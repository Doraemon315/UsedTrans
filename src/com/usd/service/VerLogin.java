package com.usd.service;

import java.util.List;

import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.usd.bean.AdminBean;
import com.usd.bean.Commodity;
import com.usd.bean.LoginBean3;
import com.usd.db.DbProvider;
import com.usd.serviceinter.BasicProviderInter;
import com.usd.utils.Myutils;

public class VerLogin implements BasicProviderInter {

	@Override
	public List<LoginBean3> queryVer(LoginBean3 bean) {
		// 查询用户与密码语句
		String sql = "select * from info where Iphone=? and Pass=?";
		System.out.println(bean.getIphone() + bean.getPass());
		List<LoginBean3> list = Myutils.getInstence(DbProvider.class).query(sql,
				new BeanListHandler<>(LoginBean3.class), bean.getIphone(), bean.getPass());
		return list;
	}

	@Override
	public List<AdminBean> queryAdmin(AdminBean bean) {
		// 查询用户与密码语句
		String sql = "select * from admininfo where adminName=? and adminPasd=?";
		List<AdminBean> list = Myutils.getInstence(DbProvider.class).query(sql, new BeanListHandler<>(AdminBean.class),
				bean.getAdminName(), bean.getAdminPasd());
		return list;
	}

	public int saveOrUpdateAdminInf(LoginBean3 adminB) {
		// 默认返回值
		int i = 0;
		// 新增和修改的区别在于：参数实体类的主键字段是否为null，为null则是新增操作，不为null则是修改操作
		// 新增的sql语句
		if (adminB != null) {
			// 新增操作
			if (adminB.getIphone() != null) {
				// 新增sql语句
				String strSql = "insert into info(userName,Pass,tpass,Sex,birthday,iphone) values(?,?,?,?,to_date(?,'yyyy-mm-dd hh24:mi:ss'),?)";
				// 调用db层进行数据库操作
				i = Myutils.getInstence(DbProvider.class).upade(strSql, adminB.getUserName(), adminB.getPass(),adminB.getTpass(), adminB.getSex(),Myutils.convertDate2String(adminB.getBirthday(), "yyyy-MM-dd HH:mm:ss"), adminB.getIphone());
			} else {
				// 修改操作
			}
		}
		return i;
	}

	public int saveOrUpdateAdmin(AdminBean adminB) {
		// 默认返回值
		int i = 0;
		// 新增和修改的区别在于：参数实体类的主键字段是否为null，为null则是新增操作，不为null则是修改操作
		// 新增的sql语句
		if (adminB != null) {
			// 新增操作
			if (adminB.getAdminName() != null) {
				// 新增sql语句
				String strSql = "insert into admininfo(adminName,adminPasd，adminSex,adminUrl,adminDate) values(?,?,?,?,to_date(?,'yyyy-mm-dd hh24:mi:ss'))";
				// 调用db层进行数据库操作
				i = Myutils.getInstence(DbProvider.class).upade(strSql, adminB.getAdminName(), adminB.getAdminPasd(),
						adminB.getAdminSex(), adminB.getAdminUrl(),
						Myutils.convertDate2String(adminB.getAdminDate(), "yyyy-MM-dd HH:mm:ss"));
			} else {
				// 修改操作
			}
		}
		return i;
	}

	@Override
	public int saveOrUpdateCommodity(Commodity comm) {
		// TODO Auto-generated method stub
		// 默认返回值
		int i = 0;
		// 新增和修改的区别在于：参数实体类的主键字段是否为null，为null则是新增操作，不为null则是修改操作
		// 新增的sql语句
		if (comm != null) {
			// 新增操作
			if (comm.getCity() != null) {
				// 新增sql语句
				String strSql = "insert into commodity(commodity,iphone,picture,city,newold,position,introduce,price) values(?,?,?,?,?,?,?,?)";
				// 调用db层进行数据库操作
				i = Myutils.getInstence(DbProvider.class).upade(strSql, comm.getCommodity(), comm.getIphone(),
						comm.getPicture(), comm.getCity(), comm.getNewold(), comm.getPosition(), comm.getIntroduce(),
						comm.getPrice());
			} else {
				// 修改操作
			}
		}
		return i;
	}

	@Override
	public int deleteUserInfo(String iphone) {
		// TODO Auto-generated method stub
		// 默认返回值
		int i = 0;
		// 新增和修改的区别在于：参数实体类的主键字段是否为null，为null则是新增操作，不为null则是修改操作
		// 新增的sql语句
		if(iphone != null&&!iphone.equals("") ) {
			// 删除操作
			
				// 新增sql语句
				String strSql = "delete from info where iphone = "+iphone;
				// 调用db层进行数据库操作
				i = Myutils.getInstence(DbProvider.class).upade(strSql);
			
		}
		return i;
	}

	@Override
	public int deleteCommInfo(String commid) {
		// TODO Auto-generated method stub
		// 默认返回值
				int i = 0;
				// 新增和修改的区别在于：参数实体类的主键字段是否为null，为null则是新增操作，不为null则是修改操作
				// 新增的sql语句
				if(commid != null&&!commid.equals("") ) {
					// 删除操作
					
						// 删除sql语句
						String strSql = "delete from commodity where commid = "+commid;
						// 调用db层进行数据库操作
						i = Myutils.getInstence(DbProvider.class).upade(strSql);
					
				}
				return i;
	}
	public int joinShoppingCar(Commodity comm,String beanIphone) {
		// TODO Auto-generated method stub
		// 默认返回值
				int i = 0;
				// 新增和修改的区别在于：参数实体类的主键字段是否为null，为null则是新增操作，不为null则是修改操作
				// 新增的sql语句
				if(comm != null ) {
					// 新增操作
						// 新增sql语句
						String strSql = "insert into shoppingcar values(?,?,?,?,?,?)";
						// 调用db层进行数据库操作
						i = Myutils.getInstence(DbProvider.class).upade(strSql,beanIphone,comm.getCommodity(),comm.getPicture(),comm.getPrice(),comm.getCommid(),1011);
					
				}
				return i;
	}
	public int deleteShoppingCar(String shopid) {
		// TODO Auto-generated method stub
		// 默认返回值
				int i = 0;
				// 新增和修改的区别在于：参数实体类的主键字段是否为null，为null则是新增操作，不为null则是修改操作
				// 新增的sql语句
				if(shopid != null ) {
					// 新增操作
						// 新增sql语句
						String strSql = "delete from shoppingcar where shopid="+shopid;
						// 调用db层进行数据库操作
						i = Myutils.getInstence(DbProvider.class).upade(strSql);
					
				}
				return i;
	}
}
