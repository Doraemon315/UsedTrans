package com.usd.trans;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.usd.bean.ShoppingcarComBean;
import com.usd.service.QueryAjaxImpl;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class QueryShoppingServlet
 */
@WebServlet("/QueryShoppingServlet")
public class QueryShoppingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QueryShoppingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=utf-8");
		//从session获取当前登录手机号
		String beanIphone=(String)request.getSession().getAttribute("beanIphone");
		List<ShoppingcarComBean>beanList=Myutils.getInstence(QueryAjaxImpl.class).queryShoppingCar(beanIphone);
		String strJson = JSON.toJSONStringWithDateFormat(beanList, "yyyy-MM-dd HH:mm:ss");
		PrintWriter out = null;
		if (strJson.length() >1) {
			// 将数据返回请求端回调函数中
			out = response.getWriter();
			out.print(strJson);
			out.flush();
			out.close();
		} else {
			out = response.getWriter();
			out.print("抱歉,你的购物车是空的");
			out.flush();
			out.close();
		}
	}

}
