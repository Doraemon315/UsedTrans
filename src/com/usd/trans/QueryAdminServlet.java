package com.usd.trans;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usd.bean.AdminBean;
import com.usd.service.VerLogin;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class QueryAdminServlet
 */
@WebServlet("/QueryAdminServlet")
public class QueryAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QueryAdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 设置请求的编码格式
				request.setCharacterEncoding("utf-8");
				// 创建存储数据的载体
				AdminBean bean = null;
				// 获取表单属性值
				// 获得表单中input框中的值
				bean = Myutils.assignBean(request, AdminBean.class);
				// 调用model层进行业务逻辑处理：查询数据库进行用户信息合法性验证
				List<AdminBean> listBean = Myutils.getInstence(VerLogin.class).queryAdmin(bean);
				for (AdminBean adminBean : listBean) {
					//获取头像
					String beanUrl=adminBean.getAdminUrl();
					request.getSession().setAttribute("beanUrl", beanUrl);
					//获取账户名
					String beanName=adminBean.getAdminName();
					request.getSession().setAttribute("beanName", beanName);
				}
				// 验证密码是否正确
				if (listBean != null && listBean.size() > 0) {
					request.getRequestDispatcher("WEB-INF/admin/main.jsp").forward(request, response);
				} else {
					response.sendRedirect("turnAdmin.jsp");
				}
	}

}
