(function( $ ) {
	$.fn.simpleSidebar = function( options ) {
		var sbw, align, callbackA, callbackB,
			defaults  = {
				settings: {
					opener: undefined,
					wrapper: undefined, 
					ignore: undefined,
					data: 'ssbplugin',
					animation: {
						duration: 500, 
						easing: 'swing'
					}
				},
				sidebar: {
					align: undefined,
					width: 350, 
					gap: 64, 
					closingLinks: 'a',
					style: {
						zIndex: 3000
					}
				},
				mask: {
					style: {
						backgroundColor: 'black',
						opacity: 0.5,
						filter: 'Alpha(opacity=50)' 
					}
				}
			},
			config    = $.extend( true, defaults, options ),
			$sidebar  = this,
			$opener   = $( config.settings.opener ),
			$wrapper  = $( config.settings.wrapper ),
			$ignore   = $( config.settings.ignore ),
			dataName  = config.settings.data,
			duration  = config.settings.animation.duration,
			easing    = config.settings.animation.easing,
			defAlign  = config.sidebar.align,
			sbMaxW    = config.sidebar.width,
			gap       = config.sidebar.gap,
			$links    = config.sidebar.closingLinks,
			defStyle  = config.sidebar.style,
			maskDef   = config.mask.style,
			winMaxW   = sbMaxW + gap,
			
			$fixedEl  = $( '*' )
				.not( $ignore )
				.not( $sidebar )
				.filter(function() {
					return $( this ).css( 'position' ) == 'fixed';
				}),
			$absolEl  = $( '*' )
				.not( $ignore )
				.filter(function() {
					return $( this ).css( 'position' ) == 'absolute';
				}),
		
			$elements = $fixedEl
				.add( $absolEl )
				.add( $sidebar )
				.add( $wrapper )
				.not( $ignore ),
			w         = $( window ).width(),
			MaskDef = {
				position: 'fixed',
				top: -200,
				right: -200,
				left: -200,
				bottom: -200,
				zIndex: config.sidebar.style.zIndex - 1
			},
			maskStyle = $.extend( {},  maskDef, MaskDef );
		
		$sidebar
			.css( defStyle )
			.wrapInner( '<div data-' + dataName + '="sub-wrapper"></div>' );
			
		var subWrapper = $sidebar.children().filter(function() {
			return $( this ).data( dataName ) === 'sub-wrapper' ;
		});
		
		subWrapper.css({
			width: '100%',
			height: '100%',
			overflow: 'auto'
		});
		$( 'body' ).append( '<div data-' + dataName + '="mask"></div>' );
		
		var maskDiv = $( 'body' ).children().filter(function(){
			return $( this ).data( dataName ) === 'mask' ;
		});
		
		maskDiv
			.css( maskStyle )
			.hide();
		
		var animateToRight = function() {
			var nsbw = $sidebar.width();
			
			$elements.each(function() {
				$( this ).animate({
					marginLeft: '+=' + nsbw,
					marginRight: '-=' + nsbw
				}, {
					duration: duration,
					easing: easing,
					complete: callbackA
				});
			});
		},
			animateToLeft = function() {
				var nsbw = $sidebar.width();
				
				$elements.each(function() {
					$( this ).animate({
						marginLeft: '-=' + nsbw,
						marginRight: '+=' + nsbw
					}, {
						duration: duration,
						easing: easing,
						complete: callbackB
					});
				});
			},
			overflowTrue = function() {
				$( 'body, html' ).css({
					overflow: 'hidden'
				});
				
				$( maskDiv ).fadeIn();
			},
			overflowFalse = function() {
				$( maskDiv ).fadeOut(function() {
					$( 'body, html' ).css({
						overflow: 'auto'
					});
				});
			};

		if ( w < winMaxW ) {
			sbw = w - gap;
		} else {
			sbw = sbMaxW;
		}

		if( defAlign === undefined || defAlign === 'left' ) {
			align = 'left';
		} else {
			align = 'right';
		}
		
		if ( 'left' === align ) {
			$sidebar.css({
				position: 'fixed',
				top: 0,
				left: 0,
				bottom: 0,
				width: sbw,
				marginLeft: -sbw
			});
			
			callbackA = overflowTrue;
			callbackB = overflowFalse;
			
			$opener.click( animateToRight );
			
			maskDiv.click( animateToLeft );
			
			$sidebar.on( 'click', $links, animateToLeft );
		} else {
			$sidebar.css({
				position: 'fixed',
				top: 0,
				bottom: 0,
				right: 0,
				width: sbw,
				marginRight: -sbw
			});
			
			callbackA = overflowFalse;
			callbackB = overflowTrue;
			
			$opener.click( animateToLeft );
			
			maskDiv.click( animateToRight );
			
			$sidebar.on( 'click', $links, animateToRight );
		}
		
		$( window ).resize(function() {
			var rsbw, sbMar,
				w = $( this ).width();
				
			if ( w < winMaxW ) {
				rsbw = w - gap;
			} else {
				rsbw = sbMaxW;
			}
			
			$sidebar.css({
				width: rsbw
			});
			if ( 'left' === align ) {
				sbMar = parseInt( $sidebar.css( 'margin-left' ) );
				
				if ( 0 > sbMar ) {
					$sidebar.css({
						marginLeft: -rsbw
					});
				} else {
					$elements.not( $sidebar )
						.css({
							marginLeft: + rsbw,
							marginRight: - rsbw
						});
				}
			} else {
				sbMar = parseInt( $sidebar.css( 'margin-right' ) );
				
				if ( 0 > sbMar ) {
					$sidebar.css({
						marginRight: -rsbw
					});
				} else {
					$elements.not( $sidebar )
						.css({
							marginLeft: - rsbw,
							marginRight: + rsbw
						});
				}
			}
		});
		
		return this;
	};
})( jQuery );
