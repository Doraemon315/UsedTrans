package com.usd.trans;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.usd.bean.LoginBean3;
import com.usd.service.VerLogin;
import com.usd.utils.Myutils;

/**
 * Servlet implementation class LoginServlet2
 */
@WebServlet("/QueryServlet2")
public class QueryServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QueryServlet2() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 设置请求的编码格式
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=UTF-8");
		// 创建存储数据的载体
		LoginBean3 bean = null;
		// 获取表单属性值
		// 获得表单中input框中的值
		bean = Myutils.assignBean(request,LoginBean3.class);
		// 调用model层进行业务逻辑处理：查询数据库进行用户信息合法性验证
		List<LoginBean3> listBean = Myutils.getInstence(VerLogin.class).queryVer(bean);
		for (LoginBean3 loginBean3 : listBean) {
		String beanUser=loginBean3.getUserName();
		String beanTpass=loginBean3.getTpass();
		String beanIphone=loginBean3.getIphone();
		request.getSession().setAttribute("beanUser", beanUser);
		request.getSession().setAttribute("beanTpass", beanTpass);
		request.getSession().setAttribute("beanIphone", beanIphone);
		}
		PrintWriter out=response.getWriter();
		JSONObject json=new JSONObject();
		// 验证密码是否正确
		if (listBean != null && listBean.size() > 0) {
			//request.getRequestDispatcher("WEB-INF/logined/indexMain.jsp").forward(request, response);
			//json.put("status", 1);
			//out.print(json);
			out.print("1");
		} else {
			//json.put("status", 2);
			//out.print(json);
			out.print("3");
		//response.sendRedirect("turn.jsp");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
